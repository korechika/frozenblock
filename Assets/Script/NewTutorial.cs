﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class NewTutorial : MonoBehaviour
{
	struct BlockInfo {
		private GameObject obj;
		private int oldpos;
		private int newpos;

		public void set_obj (GameObject var) {
			obj = var;
		}
		public void set_oldpos (int var) {
			oldpos = var;
		}
		public void set_newpos (int var) {
			newpos = var;
		}
		public GameObject get_obj () {
			return obj;
		}
		public int get_oldpos () {
			return oldpos;
		}
		public int get_newpos () {
			return newpos;
		}
	}

	private int m_TutorialState = 0;
//	private Button m_NextButton, m_BackButton;
	private RawImage[] tutorial;
	public static int GameMode = 4;
	public static int BlockNum = 16;
	private BlockInfo[] m_blocks;
	private int m_BlockLength;
	private int m_BombMaxNum = 4;
	public GameObject BlueBlock, RedBlock, YelloBlock, GreenBlock, BlackBlock, ClearBlock;
	public GameObject UnderPlate;
	public GameObject Bomb, BombSpace;
	private Vector3[] m_Pos, m_BombPos;
	private GameObject m_LeftBlock, m_RightBlock, m_DownBlock, m_UpBlock, m_DownBlackBlock, m_ClearBlock;
	private GameObject m_UnderPlate, m_BombSpaceObj;
	private GameObject[] m_BombObj;
	public static Color originalcolor;
	private GameObject background;
	public AudioClip AudioFlick, AudioBomb, AudioBombUp, AudioLevelUp, AudioNoBomb;
	AudioSource FlickSound, BombSound, BombUpSound, LevelUpSound, NoBombSound;
	public static int m_BombCounter = 0;
	public Canvas canvas;
	public static int Score = 0;
	public static int NowLevel = 0;
	public static int MaxScore = 0;
	public static int UserLevel = 0;

	private float m_MoveSpeed = 0.05f;
	private float m_LeftTime;
	public float LimitTime = 5.0f;
	private float m_LoopWaitTime = 0.02f;

	private bool[] m_BlockisOn;
//	private bool m_SwipeStart = false;
//	private string m_SwipeResult = null;
	private Vector3 m_SwipeStartPos, m_SwipeStopPos;
	private GameObject m_TouchObj;
	private int[] m_SpawnBlockPositions, m_tmpPosition1, m_tmpPosition2, m_tmpPosition3;
	private int[] m_SpawnBlockDirections;

	private bool m_GameStartflg = false;
	private int m_SpawnNum = 2;
	private bool m_spawnNumUpFlg = false, m_spawnNumUpFlg2 = false, m_spawnNumUpFlg3 = false, m_spawnNumUpFlg4 = false;
	private int m_BombCount = 0;
	private int m_BombUpSpan = 10;
	public static int MoveCount = 0;
	private int m_OnBlockNum = 0;
	public bool AllRemoveFlg = true;
	private Slider[] slider ;
	private Color m_AllRemoveColor;

	void Start ()
	{
		//stageSet ();
		int i;

		tutorial = new RawImage[5];
		RawImage[] rawimage = canvas.GetComponentsInChildren<RawImage> ();
		foreach (RawImage raw in rawimage) {
			if (raw.name == "Tutorial1")
				tutorial [0] = raw;
			if (raw.name == "Tutorial2_1")
				tutorial [1] = raw;
			if (raw.name == "Tutorial2_2")
				tutorial [2] = raw;
			if (raw.name == "Tutorial3")
				tutorial [3] = raw;
			if (raw.name == "Tutorial4")
				tutorial [4] = raw;
		}
		for (i = 1; i < 5; i++) {
			tutorial [i].gameObject.SetActive (false);
		}
//		Button[] button = canvas.GetComponentsInChildren<Button> ();
//		foreach (Button but in button) {
//			if (but.name == "BackButton")
//				m_BackButton = but;
//			if (but.name == "NextButton")
//				m_NextButton = but;
//		}
//		m_BackButton.gameObject.SetActive (false);
	}

	public void NextButtonAction () {
		tutorial [m_TutorialState].gameObject.SetActive (false);
		m_TutorialState++;
//		if (m_TutorialState == 1)
//			m_BackButton.gameObject.SetActive (true);
		if (m_TutorialState != 5) {
			tutorial [m_TutorialState].gameObject.SetActive (true);
		} else {
			Application.LoadLevel ("Title");
		}
		if (m_TutorialState == 4) {
			Button[] button = canvas.GetComponentsInChildren<Button> ();
			foreach (Button but in button) {
				if (but.name == "NextButton")
					but.GetComponentInChildren<Text> ().text = "終了";
			}
		}
	}
	public void BackButtonAction() {
		tutorial [m_TutorialState].gameObject.SetActive (false);
		m_TutorialState--;
		if (m_TutorialState == 3) {
			Button[] button = canvas.GetComponentsInChildren<Button> ();
			foreach (Button but in button) {
				if (but.name == "NextButton")
					but.GetComponentInChildren<Text> ().text = "次へ";
			}
		}
		if (m_TutorialState == 0) {
			Button[] button = canvas.GetComponentsInChildren<Button> ();
			foreach (Button but in button) {
				if (but.name == "BackButton")
					but.gameObject.SetActive (false);
			}
		}
		tutorial [m_TutorialState].gameObject.SetActive (true);
	}

	void Update ()
	{
		/*
		uguiSet ();
		ScoreChecker ();
		checkBlockNum (out m_OnBlockNum);
		if (!m_GameStartflg)
			if (Input.GetMouseButtonDown (0))
				m_GameStartflg = true;

		if (m_GameStartflg) {
			if (!m_TimeStopFlg) {
				m_LeftTime -= Time.deltaTime;
			}
			m_SwipeResult = null;

			// スワイプの検知
			if (Input.GetMouseButtonDown (0)) {
				MoveCount = 0;
				m_SwipeStartPos = Input.mousePosition;
				m_SwipeStart = true;
				RaycastScrollListner (Input.mousePosition, out m_TouchObj);
			} else if (Input.GetMouseButton (0) && m_SwipeStart) {
				m_SwipeStopPos = Input.mousePosition;
				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
				if (m_SwipeResult != null) {
					swipeArrower (m_SwipeResult, 1);
					StartCoroutine (swipetwice(m_SwipeResult));
					m_SwipeResult = null;
					m_SwipeStart = false;
				}
			} else if (Input.GetMouseButtonUp (0) && (m_TouchObj.name == "BlueBlock(Clone)" || m_TouchObj.name == "YellowBlock(Clone)" ||
				m_TouchObj.name == "GreenBlock(Clone)" || m_TouchObj.name == "RedBlock(Clone)") && m_SwipeStart) {
				BombAction (m_TouchObj);
				m_SwipeStart = false;
			}
		}
		*/
	}

	// スコアごとの処理
	private void ScoreChecker () {
		if (m_BombCounter / m_BombUpSpan > 0) {
			m_BombCounter -= m_BombUpSpan;
			if (LimitTime > 4f && m_spawnNumUpFlg) {
				LimitTime -= 0.2f;
			}
			else if (LimitTime > 3f && m_spawnNumUpFlg2) {
				LimitTime -= 0.1f;
			}
			else if (LimitTime > 2f && m_spawnNumUpFlg3) {
				LimitTime -= 0.1f;
			}
			else if (LimitTime > 1.5f && m_spawnNumUpFlg4) {
				LimitTime -= 0.02f;
			}
			BombAdder ();
		}
	}
		
	// ブロックの出現場所を m_SpawnBlockPositions[i] に入れる（最大出現数３）
	private void spawnPosSetter () {
		int i = 0, j = 0, k = 0, l = 0, m = 0;

		for (i = 0; i < BlockNum; i++) {
			m_SpawnBlockPositions[i] = 0;
			if (m_BlockisOn [i] == false) {
				m_tmpPosition1 [j] = i;
				j++;
			}
		}
		if (j > 0) {
			for (i = 0; i < m_SpawnNum; i++) {
				m_SpawnBlockDirections [i] = Random.Range (0, 4);
			}
			m_SpawnBlockPositions [0] = m_tmpPosition1 [Random.Range (0, j)];
			for (k = 1; k < m_SpawnNum; k++) {
				if (k == 1) {
					for (i = 0; i < j; i++) {
						if (m_tmpPosition1 [i] % m_BlockLength != m_SpawnBlockPositions [k - 1] % m_BlockLength
						    && m_tmpPosition1 [i] / m_BlockLength != m_SpawnBlockPositions [k - 1] / m_BlockLength) {
							m_tmpPosition2 [l] = m_tmpPosition1 [i];
							l++;
						}
					}
					if (l != 0) {
						m_SpawnBlockPositions [k] = m_tmpPosition2 [Random.Range (0, l)];
					} else if (l == 0) {
						for (i = 0; i < j; i++) {
							if (m_tmpPosition1 [i] != m_SpawnBlockPositions [0]) {
								m_tmpPosition2 [l] = m_tmpPosition1 [i];
								l++;
							}
						}
						if (l != 0) {
							m_SpawnBlockPositions [k] = m_tmpPosition2 [Random.Range (0, l)];
							for (i = 0; i < l; i++) {
								if (m_tmpPosition2 [i] != m_SpawnBlockPositions [k]) {
									m_tmpPosition3 [m] = m_tmpPosition2 [i];
									m++;
								}
								if (m != 0) {
									m_SpawnBlockPositions [k + 1] = m_tmpPosition3 [Random.Range (0, m)];
								} else if (m == 0) {
									m_SpawnBlockPositions [k + 1] = -1;
								}
							}
						} else {
							m_SpawnBlockPositions [k] = -1;
							m_SpawnBlockPositions [k + 1] = -1;
						}
					}
				}

				if (k == 2 && m_SpawnBlockPositions [k] != -1) {
					for (i = 0; i < l; i++) {
						if (m_tmpPosition2 [i] % m_BlockLength != m_SpawnBlockPositions [k - 1] % m_BlockLength
						    && m_tmpPosition2 [i] / m_BlockLength != m_SpawnBlockPositions [k - 1] / m_BlockLength) {
							m_tmpPosition3 [m] = m_tmpPosition2 [i];
							m++;
						}
					}
					if (m != 0)
						m_SpawnBlockPositions [k] = m_tmpPosition3 [Random.Range (0, m)];
					if (m == 0) {
						for (i = 0; i < l; i++) {
							if (m_tmpPosition2 [i] != m_SpawnBlockPositions [k - 1]) {
								m_tmpPosition3 [m] = m_tmpPosition2 [i];
								m++;
							}
						}
						if (m != 0)
							m_SpawnBlockPositions [k] = m_tmpPosition3 [Random.Range (0, m)];
						else if (m == 0)
							m_SpawnBlockPositions [k] = -1;
					}
				}
			}
		} else if (j == 0)
			for (i = 0; i < m_SpawnNum; i++)
				m_SpawnBlockPositions [i] = -1;
	} 
		
	// タッチした物体をtouchObjとして返す
	private bool RaycastScrollListner(Vector3 point, out GameObject touchObj)
	{
		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		Ray ray = playercamera.ScreenPointToRay (point);
		RaycastHit hit;
		if (Physics.Raycast (ray.origin, ray.direction, out hit, (playercamera.farClipPlane - playercamera.nearClipPlane), 1 << gameObject.layer)) {
			touchObj = hit.collider.gameObject;
			return true;
		}
		touchObj = null;
		return false;
	}

	// ブロックを動かす
	private void moveBlock (GameObject obj, int oldpos, int newpos) {
		LeftTimeInit ();
		if (newpos >= 0) {
			m_BlockisOn [oldpos] = false;
			m_BlockisOn [newpos] = true;
			iTween.MoveTo (obj, m_Pos [newpos], m_MoveSpeed);
		} else if (newpos < 0) {
			MoveCountAdder ();
			ScoreAdder ();
			m_BlockisOn [oldpos] = false;
			obj.GetComponent<Animator> ().SetTrigger ("Movetrigger");
		}
	}

	// 複数のブロックを moveBlock に渡す
	private void moveBlocks (int blocknum, int count) {
		int i = 0;

		for (i = 0; i < blocknum; i++) {
			moveBlock (m_blocks [i].get_obj (), m_blocks [i].get_oldpos (), m_blocks [i].get_newpos ());
		}
		if (MoveCount >= 3 && count == m_BlockLength) {
			StartCoroutine ("RemoveAction");
		}
	}

	private IEnumerator swipetwice(string swiperesult) {
		int i = 0;
		for (i = 1; i < m_BlockLength; i++) {
			yield return new WaitForSeconds (m_LoopWaitTime);
			swipeArrower (swiperesult, i + 1);
		}
	}

	// スワイプ方向にあるブロックの情報を moveBlocks に渡す
	private void swipeArrower (string direction, int count) {
		int i = 0, j = 0;

		if (direction.Equals("Right")) {
			List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Right"));
			List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("LeftBlack"));
			tempobjs1.AddRange (tempobjs2);
			foreach (GameObject touchObj in tempobjs1) {
				for (i = 0; i < BlockNum; i++) {
					if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
				//if (m_Pos [i] == objs.transform.position)
				break;
				}
				if (i >= 0 && i < 16 && m_BlockisOn[i])
				if (BlockNum == 16) {
					if (i == 3 || i == 7 || i == 11 || i == 15) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
						//moveBlock (touchObj, i, -1);
					} else if (!m_BlockisOn [i + 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
						//moveBlock (touchObj, i, -1);
					} else if (!m_BlockisOn [i + 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
						if (!m_BlockisOn [i + 2]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
							//moveBlock (touchObj, i, -1);
						} else {
							m_blocks [j].set_newpos (i + 1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
							//moveBlock (touchObj, i, i + 1);
						}
					} else if (!m_BlockisOn [i + 1] && (i == 0 || i == 4 || i == 8 || i == 12)) {
						if (!m_BlockisOn [i + 2]) {
							if (!m_BlockisOn [i + 3]) {
								m_blocks [j].set_newpos (-1);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
								//moveBlock (touchObj, i, -1);
							} else {
								m_blocks [j].set_newpos (i + 2);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
								//moveBlock (touchObj, i, i + 2);
							}
						} else {
							m_blocks [j].set_newpos (i + 1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
							//moveBlock (touchObj, i, i + 1);
						}
					} 
				}
				if (j == 1 && count == 1)
					FlickSound.Play ();
			}

		} else if (direction.Equals("Left")) {
			List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Left"));
			List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("RightBlack"));
			tempobjs1.AddRange (tempobjs2);
			foreach (GameObject touchObj in tempobjs1) {
				for (i = 0; i < BlockNum; i++) {
					if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
						//if (m_Pos [i] == objs.transform.position)
						break;
				}
				if (i >= 0 && i < 16 && m_BlockisOn[i])
				if (BlockNum == 16) {
					if (i == 0 || i == 4 || i == 8 || i == 12) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i - 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i - 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
						if (!m_BlockisOn [i - 2]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i - 1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else if (!m_BlockisOn [i - 1] && (i == 3 || i == 7 || i == 11 || i == 15)) {
						if (!m_BlockisOn [i - 2]) {
							if (!m_BlockisOn [i - 3]) {
								m_blocks [j].set_newpos (-1);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							} else {
								m_blocks [j].set_newpos (i - 2);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							}
						} else {
							m_blocks [j].set_newpos (i - 1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					}
				}
				if (j == 1 && count == 1)
					FlickSound.Play ();
			}

		} else if (direction.Equals("Up")) {
			List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
			List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("DownBlack"));
			tempobjs1.AddRange (tempobjs2);
			foreach (GameObject touchObj in tempobjs1) {
				for (i = 0; i < BlockNum; i++) {
					if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
						//if (m_Pos [i] == objs.transform.position)
						break;
				}
				if (i >= 0 && i < 16 && m_BlockisOn[i])
				if (BlockNum == 16) {
					if (0 <= i && i <= 3) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i - 4] && (4 <= i && i <= 7)) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i - 4] && (8 <= i && i <= 11)) {
						if (!m_BlockisOn [i - 8]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i - 4);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else if (!m_BlockisOn [i - 4] && (12 <= i && i <= 15)) {
						if (!m_BlockisOn [i - 8]) {
							if (!m_BlockisOn [i - 12]) {
								m_blocks [j].set_newpos (-1);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							} else {
								m_blocks [j].set_newpos (i - 8);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							}
						} else {
							m_blocks [j].set_newpos (i - 4);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					}
				}
				if (j == 1 && count == 1)
					FlickSound.Play ();
			}

		} else if (direction.Equals("Down")) {
			List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Down"));
			List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("UpBlack"));
			tempobjs1.AddRange (tempobjs2);
			foreach (GameObject touchObj in tempobjs1) {
				for (i = 0; i < BlockNum; i++) {
					if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
						//if (m_Pos [i] == objs.transform.position)
						break;
				}
				if (i >= 0 && i < 16 && m_BlockisOn[i])
				if (BlockNum == 16) {
					if (12 <= i && i <= 15) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i + 4] && (8 <= i && i <= 11)) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else if (!m_BlockisOn [i + 4] && (4 <= i && i <= 7)) {
						if (!m_BlockisOn [i + 8]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i + 4);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else if (!m_BlockisOn [i + 4] && (0 <= i && i <= 3)) {
						if (!m_BlockisOn [i + 8]) {
							if (!m_BlockisOn [i + 12]) {
								m_blocks [j].set_newpos (-1);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							} else {
								m_blocks [j].set_newpos (i + 8);
								m_blocks [j].set_obj (touchObj);
								m_blocks [j].set_oldpos (i);
								j++;
							}
						} else {
							m_blocks [j].set_newpos (i + 4);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					}
				}
				if (j == 1 && count == 1)
					FlickSound.Play ();
			}
		}
		if (j == 0 && count == 1) {
			StartCoroutine ("DirectionNotMatch");
		} else {
			moveBlocks (j, count);
			if (count == 1) 
				StartCoroutine ("appearArrowBlock");
			// 全消し追加
			if (count == m_BlockLength && m_OnBlockNum == 16 && AllRemoveFlg) {
				StartCoroutine ("AllBlockRemove");
			}
		}
	}

	// 終了判定
	private void checkBlockNum (out int checker) {
		checker = 0;
		foreach (bool check in m_BlockisOn) {
			if (!check) {
				checker++;
			}
		}
		if (checker == 0) {
			if (m_LeftTime <= 0.1)
				StartCoroutine ("EndGame");
		}
	}

	// 全消し時の動作
	private IEnumerator AllBlockRemove () {
		Score = Score + 5;
		m_BombCounter = m_BombCounter + 5;
		background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
		yield return new WaitForSeconds (0.1f);
		background.GetComponent<Renderer> ().material.color = originalcolor;
		yield return new WaitForSeconds (0.1f);
		background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
		yield return new WaitForSeconds (0.3f);
		background.GetComponent<Renderer> ().material.color = originalcolor;
	}

	// ボム使用時の動作
	private void BombAction (GameObject touchObj) {
		int i;

		if (m_BombCount > 0) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					break;
			}
			LeftTimeInit ();
			m_BlockisOn [i] = false;
			BombDowner ();
			touchObj.SetActive (false);
		} else if (m_BombCount == 0) {
			NoBombSound.Play ();
		}
	}
		
	// スワイプした方向を返す
	private void swipeDirectionChecker (Vector3 startPos, Vector3 stopPos, out string swipeResult)
	{
		float moveX = stopPos.x - startPos.x;
		float moveY = stopPos.y - startPos.y;

		if (moveX > 20 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Right";
		} else if (moveX < -20 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Left";
		} else if (moveY > 20 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Up";
		} else if (moveY < -20 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Down";
		} else {
			swipeResult = null;
		}
	}

	// ブロックの出現　＆＆　透明ブロックの出現
	private IEnumerator appearArrowBlock () {
		int i = 0;
		yield return new WaitForSeconds (0.1f);

		// ブロック出現
		for (i = 0; i < m_SpawnNum; i++) {
			if (m_SpawnBlockPositions [i] >= 0) {
				if (m_BlockisOn [m_SpawnBlockPositions [i]] == false) {
					m_BlockisOn [m_SpawnBlockPositions [i]] = true;
					spawnBlockPos (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
					//spawnBlockPos (1, m_SpawnBlockPositions [i]);
				} else
					Debug.Log ("のせたいところにブロックが既にあり spawn できない");
			}
		}
		// 透明ブロック非表示
		foreach (GameObject block in UnityEngine.GameObject.FindGameObjectsWithTag ("Clear")) {
			block.SetActive (false);
		}
		if (!m_spawnNumUpFlg2) {
			// 出現個数 3 -> 2、2 -> 3
			if (m_spawnNumUpFlg) {
				if (m_SpawnNum == 2) {
					m_SpawnNum = 3;
				} else if (m_SpawnNum == 3) {
					m_SpawnNum = 2;
				}
			}
		} else if (m_spawnNumUpFlg2) {
			m_SpawnNum = 3;
		}
		// 次の透明ブロックの表示
		spawnPosSetter ();
		for (i = 0; i < m_SpawnNum; i++) {
			if (m_SpawnBlockPositions [i] >= 0)
				spawnClearBlock (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
		}
		yield break;
	}

	// スワイプ方向にブロックが無い場合の処理
	private IEnumerator DirectionNotMatch () {
		background.GetComponent<Renderer> ().material.color = Color.yellow;
		if (GameMode == 2)
			StartCoroutine("appearArrowBlock");
		// Handheld.Vibrate();
		yield return new WaitForSeconds (0.1f);
		background.GetComponent<Renderer> ().material.color = originalcolor;
		yield break;
	}

	// ステージ生成
	private void stageSet ()
	{
		int i;
	
		NowLevel = 0;
		if (GameMode == 2) {
			m_BombMaxNum = 2;
			LimitTime = 3;
			m_spawnNumUpFlg2 = true;
			NowLevel = 5;
		}
		Score = 0;
		m_BombCounter = 0;
		m_BlockLength = (int)Mathf.Sqrt (BlockNum);
		m_BlockisOn = new bool[BlockNum];
		m_Pos = new Vector3[BlockNum];
		m_BombPos = new Vector3[m_BombMaxNum];
		m_BombObj = new GameObject[m_BombMaxNum];
		m_SpawnBlockPositions = new int[BlockNum];
		m_tmpPosition1 = new int[BlockNum];
		m_tmpPosition2 = new int[BlockNum];
		m_tmpPosition3 = new int[BlockNum];
		m_SpawnBlockDirections = new int[4] {0, 0, 0, 0};
		m_AllRemoveColor = new Color ((float)153 / 255, 1f, (float)102 / 255, 0.5f);

		AudioSource[] audioSources = GetComponents<AudioSource> ();
		FlickSound = audioSources [0];
		BombSound = audioSources [1];
		BombUpSound = audioSources [2];
		LevelUpSound = audioSources [3];
		NoBombSound = audioSources [4];
		FlickSound.clip = AudioFlick;
		BombSound.clip = AudioBomb;
		BombUpSound.clip = AudioBombUp;
		LevelUpSound.clip = AudioLevelUp;
		NoBombSound.clip = AudioNoBomb;
		m_LeftTime = LimitTime;

		m_blocks = new BlockInfo[BlockNum];
		background = GameObject.Find ("BackGround");
		originalcolor = background.GetComponent<Renderer> ().sharedMaterial.color;
		background.GetComponent<Renderer> ().material.color = originalcolor;
		slider = canvas.GetComponentsInChildren<Slider> ();

		// 初期化
		for (i = 0; i < BlockNum; i++) {
			m_BlockisOn [i] = false;
			m_Pos [i] = new Vector3 (0f, 0f, 0f);
			if (i < m_BombMaxNum) 
				m_BombPos [i] = new Vector3 (0f, 0f, 0f);
			m_SpawnBlockPositions [i] = 0;
			m_tmpPosition1 [i] = 0;
			m_tmpPosition2 [i] = 0;
			m_tmpPosition3 [i] = 0;
			m_blocks [i].set_obj (null);
			m_blocks [i].set_oldpos (-1);
			m_blocks [i].set_newpos (-1);
		}

		//m_Pos [0] = this.transform.position;
		m_Pos [0] = Vector3.right *  this.transform.position.x + Vector3.up * this.transform.position.y + Vector3.forward * (this.transform.position.z);
		for (i = 1; i < BlockNum; i++) {
			m_Pos [i] = Vector3.right * (m_Pos [i - 1].x + 1.0f) + Vector3.up * m_Pos [i - 1].y + Vector3.forward * m_Pos [i - 1].z;
			if (i % (int)Mathf.Sqrt((float)BlockNum) == 0)
				m_Pos [i] = Vector3.right * (m_Pos [i - 1].x - ((int)Mathf.Sqrt((float)BlockNum ) - 1) * 1.0f) + Vector3.up * m_Pos [i - 1].y + Vector3.forward * (m_Pos [i - 1].z - 1.0f);
		}
		for (i = 0; i < m_BombMaxNum; i++) 
				m_BombPos [i] = Vector3.right * m_Pos [i].x + Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos[i].z + 1.3f);

		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		if (BlockNum == 16)
			playercamera.transform.position = Vector3.right * (m_Pos [0].x + 1.5f) + Vector3.up * playercamera.transform.position.y + Vector3.forward * (m_Pos [0].z - 1.2f);

		for (i = 0; i < BlockNum; i++) {
			m_UnderPlate = (GameObject)Instantiate (UnderPlate);
			m_UnderPlate.tag = "UnderPlate";
			m_UnderPlate.transform.position = m_Pos[i] +  Vector3.up * (m_Pos[i].y - 0.1f);
		}
		BombSet ();
		BombSpawn (m_BombCount, 1);
		spawnPosSetter ();
		for (i = 0; i < m_SpawnNum; i++)
			m_SpawnBlockDirections [i] = Random.Range (0, 4);
		StartCoroutine ("appearArrowBlock");
	}

	// uGUI用
	private void uguiSet() {
		foreach (Text text in canvas.GetComponentsInChildren<Text>()) {
			if (text.name == "Score")
				text.text = "" +Score;
			if (text.name == "MaxScore" && GameMode != 3)
				text.text = "" +MaxScore;
			if (text.name == "Level" && GameMode != 3)
				text.text = "" +NowLevel;
		}
		//Slider slider = canvas.GetComponentInChildren<Slider> ();
		foreach (Slider slid in slider) {
			if (slid.name == "Slider") {
				slid.value = m_LeftTime / LimitTime;
				if (m_LeftTime <= 0) {
					LeftTimeInit ();
					StartCoroutine ("appearArrowBlock");
				}
			}
			if (slid.name == "ScoreSlider") {
				slid.value = (float)m_BombCounter / m_BombUpSpan;
			}
		}
	}

	// ブロックの Instantiate
	private void spawnClearBlock (int direction, int position) {
		switch (direction) {
		case 0:
			m_ClearBlock = (GameObject)Instantiate (ClearBlock);
			m_ClearBlock.tag = "Clear";
			m_ClearBlock.transform.eulerAngles = Vector3.up * 180f;
			m_ClearBlock.transform.position = m_Pos [position];
			break;
		case 1:
			m_ClearBlock = (GameObject)Instantiate (ClearBlock);
			m_ClearBlock.tag = "Clear";
			m_ClearBlock.transform.eulerAngles = Vector3.up * 270f;
			m_ClearBlock.transform.position = m_Pos [position];
			break;
		case 2:
			m_ClearBlock = (GameObject)Instantiate (ClearBlock);
			m_ClearBlock.tag = "Clear";
			m_ClearBlock.transform.eulerAngles = Vector3.up * 0f;
			m_ClearBlock.transform.position = m_Pos [position];
			break;
		case 3:
			m_ClearBlock = (GameObject)Instantiate (ClearBlock);
			m_ClearBlock.tag = "Clear";
			m_ClearBlock.transform.eulerAngles = Vector3.up * 90f;
			m_ClearBlock.transform.position = m_Pos [position];
			break;
		}
	}
	private void spawnBlockPos (int direction, int position)
	{

		switch (direction) {
		case 0:
			m_UpBlock = (GameObject)Instantiate (BlueBlock);
			m_UpBlock.tag = "Up";
			m_UpBlock.transform.eulerAngles = Vector3.up * 180f;
			m_UpBlock.transform.position = m_Pos [position];
			break;
		case 1:
			m_RightBlock = (GameObject)Instantiate (RedBlock);
			m_RightBlock.tag = "Right";
			m_RightBlock.transform.eulerAngles = Vector3.up * 270f;
			m_RightBlock.transform.position = m_Pos[position];
			break;
		case 2:
			m_DownBlock = (GameObject)Instantiate (YelloBlock);
			m_DownBlock.tag = "Down";
			m_DownBlock.transform.eulerAngles = Vector3.up * 0f;
			m_DownBlock.transform.position = m_Pos[position];
			break;
		case 3:
			m_LeftBlock = (GameObject)Instantiate (GreenBlock);
			m_LeftBlock.tag = "Left";
			m_LeftBlock.transform.eulerAngles = Vector3.up * 90f;
			m_LeftBlock.transform.position = m_Pos[position];
			break;
		case 4:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "UpBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 180f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 5:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "RightBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 270f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 6:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "DownBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 0f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 7:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "LeftBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 90f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		}
	}
	// ボムの初期化
	private void BombSet () {
		int i;
		for (i = 0; i < m_BombMaxNum; i++) {
			m_BombObj[i] = (GameObject)Instantiate (Bomb);
			m_BombObj[i].transform.position = m_BombPos [i];
			m_BombObj [i].SetActive (false);
			m_BombSpaceObj = (GameObject)Instantiate (BombSpace);
			m_BombSpaceObj.transform.position = m_BombPos [i];
		}
	}

	private IEnumerator RemoveAction () {
		BombAdder ();
//		if (MoveCount >= 4) {
//			StartCoroutine ("StopTime");
//		}
		yield break;
	}

	private IEnumerator LevelUp () {
		yield return new WaitForSeconds (0.3f);
		NowLevel++;
		LevelUpSound.Play ();
	}

	// ボムの Instantiate
	private void BombSpawn (int num, int flg) {
		// 2 -> BombDowner, 1 -> BombAdder
		if (flg == 2) {
			if (m_GameStartflg)
				BombSound.Play ();
			//m_BombObj [num].SetActive (false);
			m_BombObj [num].GetComponent<Animator> ().SetTrigger ("BombRemoveTrigger");
		}
		if (flg == 1) {
			if (m_GameStartflg)
				BombUpSound.Play ();
			m_BombObj [num - 1].SetActive (true);
			m_BombObj[num - 1].GetComponent <Animator> ().SetTrigger ("BombPopupTrigger");
		}
	}

	private void BombAdder () {
		if (m_BombCount < m_BombMaxNum) {
			m_BombCount++;
			BombSpawn (m_BombCount, 1);
		}
	}
	private void BombDowner () {
		m_BombCount--;
		BombSpawn (m_BombCount, 2);
	}

	// 残り時間の初期化
	public void LeftTimeInit () {
		m_LeftTime = LimitTime;
	}

	public static void MoveCountAdder () {
		MoveCount++;
	}

	// スコア更新
	public static void ScoreAdder () {
		Score++;
		m_BombCounter++;
	}

	// 最大スコア更新
	public static void MaxScoreUpdater () {
		MaxScore = Score;
	}
	// ユーザレベル更新
	public static void UserLevelUpdater () {
		UserLevel = NowLevel;
	}

	// スコアを減らす
	public static void ScoreDowner () {
		Score--;
	}

	// ボタンが押された時用
	public void reset () {
		Application.LoadLevel ("Main2");
	}
	public void title () {
		Application.LoadLevel ("Title");
	}

	// ゲームを終わらせる
	private IEnumerator EndGame () {
		//m_GameStartflg = false;
		//yield return new WaitForSeconds (1f);
		Application.LoadLevel ("End");
		yield break;
	}
}
