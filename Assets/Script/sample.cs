﻿using UnityEngine;
using System.Collections;

public class sample : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.DownArrow))
			this.GetComponent<Animator> ().SetTrigger ("Down");
		if (Input.GetKey(KeyCode.UpArrow))
			this.GetComponent<Animator> ().SetTrigger ("Up");
		if (Input.GetKey(KeyCode.RightArrow))
			this.GetComponent<Animator> ().SetTrigger ("Right");
		if (Input.GetKey(KeyCode.LeftArrow))
			this.GetComponent<Animator> ().SetTrigger ("Left");
	}
}
