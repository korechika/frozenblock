﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class End : MonoBehaviour {
	public Canvas canvas;
	private bool m_MaxScoreFlg, m_MaxTimeFlg;
	private GameObject background;

	// Use this for initialization
	void Start () {
		MaxScoreChecker ();
		uguiSet ();
	}

	private void MaxScoreChecker() {
		m_MaxScoreFlg = false;
		if (GameController.MaxScore < GameController.Score) {
			GameController.MaxScoreUpdater ();
			m_MaxScoreFlg = true;
		}
		if (GameController.MaxTime[(int)Mathf.Sqrt(GameController.BlockNum)] > GameController.NowTime) {
			GameController.MaxTimeUpDater ();
			m_MaxTimeFlg = true;
		}
		if ((int)GameController.MaxTime[(int)Mathf.Sqrt(GameController.BlockNum)] == 0) {
			GameController.MaxTimeUpDater ();
			m_MaxTimeFlg = true;
		}
	}

	private void uguiSet () {
		background = GameObject.Find ("BackGround");
		//background.GetComponent<Renderer> ().material.color = GameController.originalcolor;
		background.GetComponent<Renderer> ().material.color = Color.gray;

		foreach (Text text in canvas.GetComponentsInChildren<Text> ()) {
			if (text.name == "Score" && GameController.GameMode == 1)
				text.text = "" + (int)GameController.NowTime;
			else if (text.name == "Score" && GameController.GameMode == 2)
				text.text = "" + GameController.Score;
			else if (text.name == "MaxScore" && m_MaxScoreFlg && GameController.GameMode == 2)
				text.text = "スコア更新";
			else if (text.name == "MaxScore" && m_MaxTimeFlg && GameController.GameMode == 1)
				text.text = "タイム更新";
		}
	}

	public void toTitle () {
		Application.LoadLevel ("Title");
	}
	public void OneMore () {
		Application.LoadLevel ("Main");
	}
}
