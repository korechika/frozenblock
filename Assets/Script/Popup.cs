﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Popup : MonoBehaviour
{
	// ０：エンドレスモード、１：パズルモード
	public static int GameMode = 1;
	public static int BlockNum = 16;
	private GUIStyle style, bigstyle;
	public GameObject DownBlock, Bomb, BlackBlock;
	public GameObject FrozenBlock;
	private Vector3[] m_Pos;
	private GameObject m_LeftBlock, m_RightBlock, m_DownBlock, m_UpBlock, m_Bomb, m_DownBlackBlock;
	private GameObject m_FrozenBlock;
	private int m_SpawnCount = 0;
	float time = 0.0f;
	private float m_StartTime = 3.0f, m_LeftTime;
	public static float EndTime, TimeLimit = 20.0f;
	public float m_SpawnTime = 0.5f;
	private float m_BlackBlockSpawnTime = 15.0f;
	private bool[] m_BlockisOn;
	private bool m_SwipeStart = false;
	private string m_SwipeResult = null;
	private Vector3 m_SwipeStartPos, m_SwipeStopPos;
	private GameObject m_TouchObj;
	public static int Score = 0;
	public static int MaxScore = 0;
	private bool m_GameStartflg = false;

	// GameMode が１の場合使う
	private int m_SpawnNum = 2;

	void Start ()
	{
		stageSet ();
		guiSet ();
	}

	void Update ()
	{
		if (m_StartTime > 0 && !m_GameStartflg) {
			m_StartTime -= Time.deltaTime;
		} else if (m_StartTime < 0 && !m_GameStartflg) {
			m_GameStartflg = true;
		}

		if (m_GameStartflg) {
			time = time + Time.deltaTime;
			EndTime += Time.deltaTime;
			m_LeftTime -= Time.deltaTime;
			if (m_LeftTime < 0)
				Application.LoadLevel ("End");

			if (GameMode == 0) {
				// ブロックの出現
				if (time >= m_SpawnTime) {
					spawnBlock ();
					time = 0;
					m_SpawnCount++;
				}

				// スピードアップ
				if (m_SpawnCount > 10) {
					m_SpawnCount = 0;
					m_SpawnTime -= 0.02f;
				}
			}
			m_SwipeResult = null;

			// スワイプの検知
// 			#if UNITY_EDITOR
//			if (Input.GetMouseButtonDown (0)) {
//				m_SwipeStartPos = Input.mousePosition;
//				m_SwipeStart = true;
//
//			} else if (Input.GetMouseButtonUp (0) && m_SwipeStart) {
//				m_SwipeStopPos = Input.mousePosition;
//				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
//				m_SwipeStart = false;
//				if (m_SwipeResult != null && GameMode == 1) {
//					for (int i = 0; i < m_SpawnNum; i++) {
//						StartCoroutine ("WaitTime");
//					}
//				}
//			}
			if (Input.GetMouseButtonDown (0)) {
				m_SwipeStartPos = Input.mousePosition;
				m_SwipeStart = true;
			} else if (Input.GetMouseButton (0) && m_SwipeStart) {
				m_SwipeStopPos = Input.mousePosition;
				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
				if (m_SwipeResult != null) {
					m_SwipeResult = null;
					m_SwipeStart = false;
					for (int i = 0; i < m_SpawnNum; i++) {
						StartCoroutine ("WaitTime");
					}
				}
			}

/*			#elif UNITY_ANDROID
			if (Input.GetMouseButtonDown(0)) {
				// RaycastScrollListner (Input.mousePosition,out m_TouchObj);
				m_SwipeStartPos = Input.mousePosition;
				m_SwipeStart = true;

			} else if (Input.GetMouseButtonUp(0) && m_SwipeStart) {
				m_SwipeStopPos = Input.mousePosition;
				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
				m_SwipeStart = false;
			}
			#else
			#endif
*/
		}
	}

	private IEnumerator WaitTime() {
		yield return new WaitForSeconds (0.2f);
		spawnBlock ();
	}

	// タッチした物体をtouchObjとして返す
	private bool RaycastScrollListner(Vector3 point, out GameObject touchObj)
	{
		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		Ray ray = playercamera.ScreenPointToRay (point);
		RaycastHit hit;
		if (Physics.Raycast (ray.origin, ray.direction, out hit, (playercamera.farClipPlane - playercamera.nearClipPlane), 1 << gameObject.layer)) {
			touchObj = hit.collider.gameObject;
			return true;
		}
		touchObj = null;
		return false;
	}

	// 呼び出されたタイミングでブロックをランダムに出現　＆　終了判定
	private void spawnBlock () {
		// 0 = up , 1 = right, 2 = down , 3 = left , 4 = 爆弾
		int spawnBlockDirection;
		int spawnBlockPosition = Random.Range (0, BlockNum);
		int count = 0;

		int checker = 0;
		foreach (bool check in m_BlockisOn) {
			if (!check) {
				checker++;
			}
		}
		if (checker == 0) {
			Application.LoadLevel ("End");
		}


		while (m_BlockisOn [spawnBlockPosition]) {
			spawnBlockPosition = Random.Range (0, BlockNum);
			count++;
			if (count >= BlockNum)
				return;
		}
		// 方向の決定
		if (time > m_BlackBlockSpawnTime && Random.Range (0, 5) == 0) {
			// 黒ブロックは
			spawnBlockDirection = Random.Range (4, 8);
		} else {
			// 白ブロック
			spawnBlockDirection = Random.Range (0, 4);
		}
		spawnBlockDirection = directionPositionChecker (spawnBlockPosition, spawnBlockDirection);
		m_BlockisOn [spawnBlockPosition] = true;

		spawnBlockPos (spawnBlockDirection, spawnBlockPosition);
	}

	private void BombAction() {
		Debug.Log ("bomb touched");
	}

	// ブロックの出現
	private void spawnBlockPos (int direction, int position)
	{
		switch (direction) {
		case 0:
			m_UpBlock = (GameObject)Instantiate (DownBlock);
			m_UpBlock.GetComponent<Block> ().SetAllTag ("Up");
			m_UpBlock.transform.eulerAngles = Vector3.up * 180f;
			m_UpBlock.transform.position = m_Pos[position];
			break;
		case 1:
			m_RightBlock = (GameObject)Instantiate (DownBlock);
			m_RightBlock.GetComponent<Block> ().SetAllTag ("Right");
			m_RightBlock.transform.eulerAngles = Vector3.up * 270f;
			m_RightBlock.transform.position = m_Pos[position];
			break;
		case 2:
			m_DownBlock = (GameObject)Instantiate (DownBlock);
			m_DownBlock.GetComponent<Block> ().SetAllTag ("Down");
			m_DownBlock.transform.eulerAngles = Vector3.up * 0f;
			m_DownBlock.transform.position = m_Pos[position];
			break;
		case 3:
			m_LeftBlock = (GameObject)Instantiate (DownBlock);
			m_LeftBlock.GetComponent<Block> ().SetAllTag ("Left");
			m_LeftBlock.transform.eulerAngles = Vector3.up * 90f;
			m_LeftBlock.transform.position = m_Pos[position];
			break;
		case 4:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.GetComponent<Block> ().SetAllTag ("UpBlack");
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 180f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 5:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.GetComponent<Block> ().SetAllTag ("RightBlack");
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 270f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 6:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.GetComponent<Block> ().SetAllTag ("DownBlack");
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 0f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 7:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.GetComponent<Block> ().SetAllTag ("LeftBlack");
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 90f;
			m_DownBlackBlock.transform.position = m_Pos [position];
			break;
		case 8:
			m_Bomb = (GameObject)Instantiate (Bomb);
			m_Bomb.transform.position = m_Pos [position];
			break;
		}
	}

	// GUI：スコア表示　＆　タイトルボタン
	void OnGUI()
	{
		Rect maxScorePos = new Rect (Screen.width * 0.0f, Screen.height * 0.0f, Screen.width, Screen.height);
		Rect nowScorePos = new Rect (Screen.width * 0.4f, Screen.height * 0.8f, Screen.width, Screen.height);

		string outMessageMaxScore = "最高スコア:";
		string outMessageScore = "スコア:";
	
		outMessageMaxScore += MaxScore;
		outMessageScore += Score;
		GUI.Label (maxScorePos, outMessageMaxScore, style);
		GUI.Label (nowScorePos, outMessageScore, style);
		if ( GUI.Button(new Rect(Screen.width * 0.85f, Screen.height * 0.0f, Screen.width * 0.15f, Screen.height * 0.1f), "タイトル")) {
			Application.LoadLevel ("Title");
		}
		if ( GUI.Button(new Rect(Screen.width * 0.7f, Screen.height * 0.0f, Screen.width * 0.15f, Screen.height * 0.1f), "タイム")) {
			m_GameStartflg = false;
			m_StartTime = 10f;
		}
		if (!m_GameStartflg) {
			Rect CountDownPos = new Rect (Screen.width * 0.4f, Screen.height * 0.4f, Screen.width, Screen.height);
			string outCountDown = "";
			outCountDown += (int)m_StartTime + 1;
			GUI.Label (CountDownPos, outCountDown, bigstyle);
		}
		Rect nowLeftTime = new Rect (Screen.width * 0.0f, Screen.height * 0.1f, Screen.width, Screen.height);
		string outLeftTime = "残り時間：";
		if (m_GameStartflg) {
			outLeftTime += (int)m_LeftTime + 1;
		} else {
			outLeftTime += (int)m_LeftTime;
		}
		GUI.Label (nowLeftTime, outLeftTime, style);

	}
	void guiSet() {
		style = new GUIStyle ();
		bigstyle = new GUIStyle ();
		style.fontSize = 50;
		bigstyle.fontSize = 300;
	}

	// 詰みのチェック -> できれば削除
	private int directionPositionChecker (int pos, int direction) {
		if (BlockNum == 9) {
			if (pos == 0) {
				if (direction == 1)
					return 3;
				if (direction == 2)
					return 0;
				if (direction == 7)
					return 5;
				if (direction == 4)
					return 6;
			} else if (pos == 2) {
				if (direction == 2)
					return 0;
				if (direction == 3)
					return 1;
				if (direction == 4)
					return 6;
				if (direction == 5)
					return 7;
			} else if (pos == 6) {
				if (direction == 0)
					return 2;
				if (direction == 1)
					return 3;
				if (direction == 6)
					return 4;
				if (direction == 7)
					return 5;
			} else if (pos == 8) {
				if (direction == 0)
					return 2;
				if (direction == 3)
					return 1;
				if (direction == 6)
					return 4;
				if (direction == 5)
					return 7;

			}
		} else if (BlockNum == 16) {
			if (pos == 0 || pos == 4) {
				if (direction == 1)
					return 3;
				if (direction == 2)
					return 0;
				if (direction == 7)
					return 5;
				if (direction == 4)
					return 6;

			} else if (pos == 1) {
				if (direction == 2)
					return 1;
				if (direction == 7)
					return 5;
				if (direction == 4)
					return 6;

			} else if (pos == 3 || pos == 2) {
				if (direction == 2)
					return 0;
				if (direction == 3)
					return 1;
				if (direction == 4)
					return 6;
				if (direction == 5)
					return 7;

			} else if (pos == 7) {
				if (direction == 3)
					return 2;
				if (direction == 4)
					return 6;
				if (direction == 5)
					return 7;

			} else if (pos == 12 || pos == 13) {
				if (direction == 0)
					return 2;
				if (direction == 1)
					return 3;
				if (direction == 6)
					return 4;
				if (direction == 7)
					return 5;

			} else if (pos == 8) {
				if (direction == 1)
					return 0;
				if (direction == 6)
					return 4;
				if (direction == 7)
					return 5;

			} else if (pos == 15 || pos == 11) {
				if (direction == 0)
					return 2;
				if (direction == 3)
					return 1;
				if (direction == 6)
					return 4;
				if (direction == 5)
					return 7;

			} else if (pos == 14) {
				if (direction == 0)
					return 3;
				if (direction == 6)
					return 4;
				if (direction == 5)
					return 7;

			} else if (pos == 5 && direction == 2) {
				return 0;
			} else if (pos == 6 && direction == 3) {
				return 1;
			} else if (pos == 9 && direction == 1) {
				return 3;
			} else if (pos == 10 && direction == 0) {
				return 2;
			} else if (pos == 5 && direction == 4) {
				return 6;
			} else if (pos == 6 && direction == 5) {
				return 7;
			} else if (pos == 9 && direction == 7) {
				return 6;
			} else if (pos == 10 && direction == 6) {
				return 4;
			}

		}
		return direction;
	}
		
	// スワイプした方向を swipeResult として返す　＆　ブロックの移動を制御
	private void swipeDirectionChecker (Vector3 startPos, Vector3 stopPos, out string swipeResult)
	{
		int i = 0;
		float moveX = stopPos.x - startPos.x;
		float moveY = stopPos.y - startPos.y;

		if (moveX > 120 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			for (int j = 0; j < 2; j++) {
				List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Right"));
				List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("LeftBlack"));
				tempobjs1.AddRange (tempobjs2);
				if (tempobjs1.Count == 0)
					ScoreDowner ();
				foreach (GameObject objs in tempobjs1) {
					for (i = 0; i < BlockNum; i++) {
						if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (objs.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (objs.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
						break;
					}
					if (BlockNum == 9) {
						if (i == 2 || i == 5 || i == 8) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 1] && (i == 1 || i == 4 || i == 7)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 1] && (i == 0 || i == 3 || i == 6)) {
							if (!m_BlockisOn [i + 2]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 1] = true;
							}
						}
					} else if (BlockNum == 16) {
						if (i == 3 || i == 7 || i == 11 || i == 15) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
							if (!m_BlockisOn [i + 2]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 1] = true;
							}
						} else if (!m_BlockisOn [i + 1] && (i == 0 || i == 4 || i == 8 || i == 12)) {
							if (!m_BlockisOn [i + 2]) {
								if (!m_BlockisOn [i + 3]) {
									MoveBlock (objs, 4, true);
									m_BlockisOn [i] = false;
								} else {
									MoveBlock (objs, 2, false);	
									m_BlockisOn [i] = false;
									m_BlockisOn [i + 2] = true;
								}
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 1] = true;
							}
						}
					}
				}
			}
			swipeResult = "Right";

		} else if (moveX < -120 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			for (int j = 0; j < 2; j++) {
				List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Left"));
				List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("RightBlack"));
				tempobjs1.AddRange (tempobjs2);
				if (tempobjs1.Count == 0)
					ScoreDowner ();
				foreach (GameObject objs in tempobjs1) {
					for (i = 0; i < BlockNum; i++) {
						if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (objs.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (objs.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
						break;
					}
					if (BlockNum == 9) {
						if (i == 0 || i == 3 || i == 6) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 1] && (i == 1 || i == 4 || i == 7)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 1] && (i == 2 || i == 5 || i == 8)) {
							if (!m_BlockisOn [i - 2]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 1] = true;
							}
						}
					} else if (BlockNum == 16) {
						if (i == 0 || i == 4 || i == 8 || i == 12) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
							if (!m_BlockisOn [i - 2]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 1] = true;
							}
						} else if (!m_BlockisOn [i - 1] && (i == 3 || i == 7 || i == 11 || i == 15)) {
							if (!m_BlockisOn [i - 2]) {
								if (!m_BlockisOn [i - 3]) {
									MoveBlock (objs, 4, true);
									m_BlockisOn [i] = false;
								} else {
									MoveBlock (objs, 2, false);
									m_BlockisOn [i] = false;
									m_BlockisOn [i - 2] = true;
								}
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 1] = true;
							}
						}
					}
				}
			}
			swipeResult = "Left";

		} else if (moveY > 120 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			for (int j = 0; j < 2; j++) {
				List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
				List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("DownBlack"));
				tempobjs1.AddRange (tempobjs2);
				if (tempobjs1.Count == 0)
					ScoreDowner ();
				foreach (GameObject objs in tempobjs1) {
					for (i = 0; i < BlockNum; i++) {
						if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (objs.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (objs.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
						break;
					}
					if (BlockNum == 9) {
						if (0 <= i && i <= 2) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 3] && (3 <= i && i <= 5)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 3] && (6 <= i && i <= 8)) {
							if (!m_BlockisOn [i - 6]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 3] = true;
							}
						}
					} else if (BlockNum == 16) {
						if (0 <= i && i <= 3) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 4] && (4 <= i && i <= 7)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i - 4] && (8 <= i && i <= 11)) {
							if (!m_BlockisOn [i - 8]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 4] = true;
							}
						} else if (!m_BlockisOn [i - 4] && (12 <= i && i <= 15)) {
							if (!m_BlockisOn [i - 8]) {
								if (!m_BlockisOn [i - 12]) {
									MoveBlock (objs, 4, true);
									m_BlockisOn [i] = false;
								} else {
									MoveBlock (objs, 2, false);
									m_BlockisOn [i] = false;
									m_BlockisOn [i - 8] = true;
								}
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i - 4] = true;
							}
						}
					}
				}
			}
			swipeResult = "Up";

		} else if (moveY < -120 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			for (int j = 0; j < 2; j++) {
				List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Down"));
				List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("UpBlack"));
				tempobjs1.AddRange (tempobjs2);
				if (tempobjs1.Count == 0)
					ScoreDowner ();
				foreach (GameObject objs in tempobjs1) {
					for (i = 0; i < BlockNum; i++) {
						if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (objs.transform.position.x)) && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (objs.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
						break;
					}
					if (BlockNum == 9) {
						if (6 <= i && i <= 8) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 3] && (3 <= i && i <= 5)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 3] && (0 <= i && i <= 2)) {
							if (!m_BlockisOn [i + 6]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 3] = true;
							}
						}
					} else if (BlockNum == 16) {
						if (12 <= i && i <= 15) {
							MoveBlock (objs, 1, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 4] && (8 <= i && i <= 11)) {
							MoveBlock (objs, 2, true);
							m_BlockisOn [i] = false;
						} else if (!m_BlockisOn [i + 4] && (4 <= i && i <= 7)) {
							if (!m_BlockisOn [i + 8]) {
								MoveBlock (objs, 3, true);
								m_BlockisOn [i] = false;
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 4] = true;
							}
						} else if (!m_BlockisOn [i + 4] && (0 <= i && i <= 3)) {
							if (!m_BlockisOn [i + 8]) {
								if (!m_BlockisOn [i + 12]) {
									MoveBlock (objs, 4, true);
									m_BlockisOn [i] = false;
								} else {
									MoveBlock (objs, 2, false);	
									m_BlockisOn [i] = false;
									m_BlockisOn [i + 8] = true;
								}
							} else {
								MoveBlock (objs, 1, false);
								m_BlockisOn [i] = false;
								m_BlockisOn [i + 4] = true;
							}
						}
					}
				}
			}
			swipeResult = "Down";


		} else {
			swipeResult = null;
		}
	}

	// 受け取ったオブジェクトにアニメーターを適応
	private void MoveBlock (GameObject obj, int i, bool deleteFlg) {
		switch (i) {
		case 1:
			if (deleteFlg) {
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move1DeleteTrigger");
			} else {
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move1Trigger");
			}
			break;

		case 2:
			if (deleteFlg) {
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move2DeleteTrigger");
			} else {
				// not created yet
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move2Trigger");
			}
			break;

		case 3:
			if (deleteFlg) {
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move3DeleteTrigger");
			} else {
				// not created yet
			}
			break;

		case 4:
			if (deleteFlg) {
				obj.GetComponentInChildren<Animator> ().SetTrigger ("Move4DeleteTrigger");
			} else {
				// not created yet
			}
			break;

		default:
			break;
		}
	}

	// ステージ生成
	private void stageSet ()
	{
		int i;

		EndTime = 0.0f;
		Score = 0;
		m_LeftTime = TimeLimit;
		m_BlockisOn = new bool[BlockNum];
		m_Pos = new Vector3[BlockNum];
		for (i = 0; i < BlockNum; i++) {
			m_BlockisOn [i] = false;
			m_Pos [i] = new Vector3 (0f, 0f, 0f);
		}

		m_Pos [0] = this.transform.position;
		for (i = 1; i < BlockNum; i++) {
			m_Pos [i] = Vector3.right * (m_Pos [i - 1].x + 1.0f) + Vector3.up * m_Pos [i - 1].y + Vector3.forward * m_Pos [i - 1].z;
			if (i % (int)Mathf.Sqrt((float)BlockNum) == 0)
				m_Pos [i] = Vector3.right * (m_Pos [i - 1].x - ((int)Mathf.Sqrt((float)BlockNum ) - 1) * 1.0f) + Vector3.up * m_Pos [i - 1].y + Vector3.forward * (m_Pos [i - 1].z - 1.0f);
		}

		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		//playercamera.transform.position = Vector3.right * (playercamera.transform.position.x + 0.5f) + Vector3.up * playercamera.transform.position.y + Vector3.forward * (playercamera.transform.position.z - 0.1f);
		playercamera.transform.position = Vector3.right * (m_Pos [0].x + 1.5f) + Vector3.up * playercamera.transform.position.y + Vector3.forward * (m_Pos [0].z - 1.5f);
		for (i = 0; i < BlockNum; i++) {
			m_FrozenBlock = (GameObject)Instantiate (FrozenBlock);
			m_FrozenBlock.transform.position = m_Pos[i] +  Vector3.up * (m_Pos[i].y - 0.5f);
		}

		for (i = 0; i < BlockNum / 3; i++) {
			spawnBlock ();
		}
	}

	private void CountDown () {
		System.Threading.Thread.Sleep(10000);
	}

	public static void ScoreAdder () {
		Score++;
	}

	public static void MaxScoreUpdater () {
		MaxScore = Score;
	}

	public static void ScoreDowner () {
		Score--;
	}
}
