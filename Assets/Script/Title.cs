﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Title : MonoBehaviour {
	public Canvas canvas;
	public static int level;
	public Button HardModeButton, fiveModeButton;

	// Use this for initialization
	void Start () {
		uguiSet ();
	}

	private void uguiSet () {
		foreach (Text text in canvas.GetComponentsInChildren<Text> ()) {
			if (text.name == "Score") {
				text.text = "" + GameController.MaxScore;
			}
		}
	}

	public void Tutorial () {
		Application.LoadLevel ("Tutorial");
	}
	public void Normal () {
		GameController.GameMode = 2;
		GameController.BlockNum = 16;
		Application.LoadLevel ("Main");
	}
	public void Five () {
		GameController.GameMode = 2;
		GameController.BlockNum = 25;
		Application.LoadLevel ("Main");
	}
	public void Six () {
		GameController.GameMode = 2;
		GameController.BlockNum = 36;
		Application.LoadLevel ("Main");
	}
	public void Seven () {
		GameController.GameMode = 2;
		GameController.BlockNum = 49;
		Application.LoadLevel ("Main");
	}
	public void Eight () {
		GameController.GameMode = 2;
		GameController.BlockNum = 64;
		Application.LoadLevel ("Main");
	}
	public void Nine () {
		GameController.GameMode = 1;
		GameController.BlockNum = 81;
		Application.LoadLevel ("Main");
	}
	public void Ten () {
		GameController.GameMode = 1;
		GameController.BlockNum = 100;
		Application.LoadLevel ("Main");
	}
	public void Eleven () {
		GameController.GameMode = 1;
		GameController.BlockNum = 121;
		Application.LoadLevel ("Main");
	}
}
