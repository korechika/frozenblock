﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class GameController : MonoBehaviour
{
	struct BlockInfo
	{
		private GameObject obj;
		private int oldpos;
		private int newpos;

		public void set_obj (GameObject var)
		{
			obj = var;
		}

		public void set_oldpos (int var)
		{
			oldpos = var;
		}

		public void set_newpos (int var)
		{
			newpos = var;
		}

		public GameObject get_obj ()
		{
			return obj;
		}

		public int get_oldpos ()
		{
			return oldpos;
		}

		public int get_newpos ()
		{
			return newpos;
		}
	}

	// public
	public static int GameMode = 2;
	// 1 -> スイーパ, 2 -> パズルアロー
	public static int BlockNum = 16;
	public static Color backgroundColor, originalBackGroundColor;
	public ParticleSystem BombParticle, RefreshParticle0, RefreshParticle1, RefreshParticle2, RefreshParticle3;
	public AudioClip AudioFlick, AudioBomb, AudioBombUp, AudioLevelUp, AudioNoBomb, AudioRemoveAll;
	AudioSource FlickSound, BombSound, BombUpSound, LevelUpSound, NoBombSound, RemoveAllSound;
	public static int m_BombCounter = 0;
	public Canvas canvas;
	public static int Score = 0;
	public static int NowLevel = 0;
	public static int MaxScore = 0;
	public static float[] MaxTime = new float[] {
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f
	};
	public static float NowTime = 0.0f/*, MaxTime = 0.0f*/;
	public int BombUpSpan = 10;
	public static int BombAddNum = 3;
	public static int MoveCount = 0;
	public GameObject EndMenu, CutInMenu;


	// private 変数
	private BlockInfo[] m_blocks;
	private int m_Length;
	private int m_BombMaxNum = 4;
	private Vector3[] m_Pos, m_BombPos, m_GagePos;
	private int m_FreezeBlockNum = 0;
	private int m_RensaCount = 0;
	private float m_MoveSpeed = 0.05f;
	private float m_LoopWaitTime = 0.03f;
	private string m_SwipeResult = null;
	private Vector3 m_SwipeStartPos, m_SwipeStopPos;
	private int[] m_SpawnBlockPositions, m_tmpPosition1, m_tmpPosition2, m_tmpPosition3;
	private int[] m_SpawnBlockDirections;
	private int m_SpawnNum = 2;
	private int m_BombCount = 2;
	private int m_OnBlockNum;
	private int m_ScoreFontSize;

	// GameObject Image Text Color
	public GameObject BlueBlock, RedBlock, YelloBlock, GreenBlock, BlackBlock, ClearBlock, ClearBlue, ClearRed, ClearYellow, ClearGreen;
	public GameObject UnderPlate;
	public GameObject Bomb, BombSpace;
	private GameObject m_Block, m_DownBlackBlock, m_ClearBlock;
	private GameObject m_UnderPlate, m_BombSpaceObj;
	private GameObject[] m_BombObj;
	private GameObject background;
	private Color m_AllRemoveColor;
	private RawImage[] RawImages;
	private RawImage m_ZenkeshiImage, m_GoodImage, m_ExcellentImage, m_GreatImage, m_ScoredImage;
	private Text m_ScoreText, m_MaxScoreText, m_MaxScoreText2, m_RensaText, m_TimeText, m_MaxTimeText, m_BonusText, m_BonusScoreText;
	private GameObject m_TouchObj;
	private Color m_ScoreColor, m_originalScoreColor;

	// flg
	private bool maxScoreUpdate = false;
	private bool[] m_BlockisOn;
	private bool m_SwipeStart = false;
	private bool m_GameStartflg = false, m_EndGameflg = false;
	private bool m_spawnNumUpFlg0 = false, m_spawnNumUpFlg1 = false, m_spawnNumUpFlg2 = false
		,
		m_spawnNumUpFlg2_1 = false, m_spawnNumUpFlg3 = false, m_spawnNumUpFlg3_1 = false, m_spawnNumUpFlg4 = false
		,
		m_spawnNumUpFlg5 = false, m_spawnNumUpFlg6 = false, m_spawnNumUpFlg7 = false, m_spawnNumUpFlg8 = false
		,
		m_spawnNumUpFlg9 = false;
	private bool m_AllRemoveFlg = false;
	private bool m_NoBlockMoveFlg = false;
	private bool m_RefreshFlg = false;


	void Start ()
	{
		stageSet ();
//		CutInMenu.SetActive (true);
	}

	void Update ()
	{
		uguiSet ();
		if (GameMode == 2)
			ScoreChecker ();

		if (!m_GameStartflg) {
			if (Input.GetMouseButtonDown (0) && !m_EndGameflg) {
				m_GameStartflg = true;
//				CutInMenu.SetActive (false);
			}
		} 
		if (m_GameStartflg) {
			NowTime += Time.deltaTime;
			m_SwipeResult = null;

			// スワイプの検知
			if (Input.GetMouseButtonDown (0)) {
				MoveCount = 0;
				m_SwipeStartPos = Input.mousePosition;
				m_SwipeStart = true;
				RaycastScrollListner (Input.mousePosition, out m_TouchObj);
			} else if (Input.GetMouseButton (0) && m_SwipeStart) {
				m_SwipeStopPos = Input.mousePosition;
				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
				if (m_SwipeResult != null && m_OnBlockNum != BlockNum) {
					swipeArrower (m_SwipeResult, 1);
					StartCoroutine (swipetwice (m_SwipeResult));
					m_SwipeResult = null;
					m_SwipeStart = false;
				}
			} else if (Input.GetMouseButtonUp (0)
			           && (m_TouchObj.name == "BlueBlock(Clone)" || m_TouchObj.name == "YellowBlock(Clone)" ||
			           m_TouchObj.name == "GreenBlock(Clone)" || m_TouchObj.name == "RedBlock(Clone)") && m_SwipeStart) {
				BombAction (m_TouchObj);
				m_SwipeStart = false;
			}
		}
	}

	// 終了判定   & out 空ブロックの数
	private void checkBlockNum (out int checker)
	{
		checker = 0;
		foreach (bool check in m_BlockisOn) {
			if (!check) {
				checker++;
			}
		}

		if (GameMode == 1 && checker == BlockNum)
			StartCoroutine ("EndGamePopup");
		
		if (checkEndGame ()) {
			if (GameMode == 2 && checker == 0)
				StartCoroutine ("EndGamePopup");
			else if (GameMode == 1 && checker != BlockNum && m_BombCount == 0)
				StartCoroutine ("FailGame");
		}

		if (GameMode == 2) {
			/*if (checker == 0) {
				StartCoroutine ("EndGame");
			} else */
			if (0 < checker && checker < (int)BlockNum / 4) {
				backgroundColor = new Color ((float)204 / 255, (float)140 / 255, (float)140 / 255, backgroundColor.a);
				m_ScoreColor = Color.white;
				if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				    || background.GetComponent<Renderer> ().material.color != Color.yellow
				    || background.GetComponent<Renderer> ().material.color != Color.gray)
					background.GetComponent<Renderer> ().material.color = backgroundColor;
			} else if ((int)BlockNum / 4 < checker && checker < (int)BlockNum / 2) {
				backgroundColor = new Color ((float)204 / 255, (float)190 / 255, (float)190 / 255);
				m_ScoreColor = Color.white;
				if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				    || background.GetComponent<Renderer> ().material.color != Color.yellow
				    || background.GetComponent<Renderer> ().material.color != Color.gray)
					background.GetComponent<Renderer> ().material.color = backgroundColor;
			} else if ((int)BlockNum / 2 < checker && checker < BlockNum) {
//				backgroundColor = new Color ((float)181 / 255, (float)220 / 255, (float)231 / 255, (float)192 / 255);
				m_ScoreColor = m_originalScoreColor;
				if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				    || background.GetComponent<Renderer> ().material.color != Color.yellow)
					background.GetComponent<Renderer> ().material.color = originalBackGroundColor;
			}
		} else {
			
		}
	}

	// スコアごとにフラグをたてる
	private void ScoreChecker ()
	{
		/*if (Score >= 5 && !m_spawnNumUpFlg0) {
			m_spawnNumUpFlg0 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 10 && !m_spawnNumUpFlg1) {
			m_spawnNumUpFlg1 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 15 && !m_spawnNumUpFlg2) {
			m_spawnNumUpFlg2 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 20 && !m_spawnNumUpFlg2_1) {
			m_spawnNumUpFlg2_1 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 25 && !m_spawnNumUpFlg3) {
			m_spawnNumUpFlg3 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 30 && !m_spawnNumUpFlg3_1) {
			m_spawnNumUpFlg3_1 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 35 && !m_spawnNumUpFlg3_1) {
			m_spawnNumUpFlg4 = true;
			StartCoroutine ("RefreshStage");
		}*/
		if (Score >= 50 && !m_spawnNumUpFlg0) {
			m_spawnNumUpFlg0 = true;
			m_SpawnNum++;
		}
		if (Score >= 100 && !m_spawnNumUpFlg1) {
			m_spawnNumUpFlg1 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 200 && !m_spawnNumUpFlg2) {
			m_SpawnNum++;
			m_spawnNumUpFlg2 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 250 && !m_spawnNumUpFlg2_1) {
			m_spawnNumUpFlg2_1 = true;
		}
		if (Score >= 300 && !m_spawnNumUpFlg3) {
			m_spawnNumUpFlg3 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 350 && !m_spawnNumUpFlg3_1) {
			m_spawnNumUpFlg3_1 = true;
		}
		if (Score >= 400 && !m_spawnNumUpFlg4) {
			m_spawnNumUpFlg4 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 500 && !m_spawnNumUpFlg5) {
			m_spawnNumUpFlg5 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 600 && !m_spawnNumUpFlg6) {
			m_spawnNumUpFlg6 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 700 && !m_spawnNumUpFlg7) {
			m_spawnNumUpFlg7 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 800 && !m_spawnNumUpFlg8) {
			m_spawnNumUpFlg8 = true;
			StartCoroutine ("RefreshStage");
		}
		if (Score >= 900 && !m_spawnNumUpFlg9) {
			m_spawnNumUpFlg9 = true;
			StartCoroutine ("RefreshStage");
		}
	}

	// 次のブロックの出てくるポジションを決める Gamemode == 2 の時のみ使う
	private void spawnPosSetter ()
	{
		int[ , ] emptyBlock = new int[m_SpawnNum, BlockNum];
		int i = 0, j = 0, k = 0, EmptyNum;

		// 初期化
		for (i = 0; i < m_SpawnNum; i++) {
			for (j = 0; j < BlockNum; j++)
				emptyBlock [i, j] = -1;
			m_SpawnBlockPositions [i] = -1;
//			m_SpawnBlockDirections [i] = 0;
			m_SpawnBlockDirections [i] = Random.Range (0, 4);
			
		}
		j = 0;

		for (i = 0; i < BlockNum; i++) {
			if (m_BlockisOn [i] == false) {
				emptyBlock [0, j] = i;
				j++;
			}
		}
		EmptyNum = j;
		// １つ目の出現
		m_SpawnBlockPositions [0] = emptyBlock [0, Random.Range (0, EmptyNum)];
		for (i = 0; i < EmptyNum; i++) {
			if (emptyBlock [0, i] != m_SpawnBlockPositions [0]) {
				emptyBlock [0, k] = emptyBlock [0, i];
				k++;
			}
		}
		EmptyNum = k;
		k = 0;

		// ２つ目以降の出現アルゴリズム
		for (i = 1; i < m_SpawnNum; i++) {
			for (j = 0; j < EmptyNum; j++) {
				if (emptyBlock [i - 1, j] % m_Length != m_SpawnBlockPositions [i - 1] % m_Length
				    && emptyBlock [i - 1, j] / m_Length != m_SpawnBlockPositions [i - 1] / m_Length
				    && emptyBlock [i - 1, j] >= 0) {
					emptyBlock [i, k] = emptyBlock [i - 1, j];
					k++;
				}
			}
			// 他の列・行に出現できる
			if (k > 0) {
				m_SpawnBlockPositions [i] = emptyBlock [i, Random.Range (0, k)];
				k = 0;
				for (j = 0; j < EmptyNum; j++) {
					if (emptyBlock [0, j] != m_SpawnBlockPositions [i]) {
						emptyBlock [0, k] = emptyBlock [0, j];
						k++;
					}
				}
				EmptyNum = k;

				// 他の列・行が存在しない
			} else {
				if (EmptyNum == 0)
					break;
				else {
					m_SpawnBlockPositions [i] = emptyBlock [0, Random.Range (0, EmptyNum)];
					for (j = 0; j < EmptyNum; j++) {
						if (emptyBlock [0, j] != m_SpawnBlockPositions [i]) {
							emptyBlock [0, k] = emptyBlock [0, j];
							k++;
						}
					}
					EmptyNum = k;
				}
			}
			k = 0;
		}

		for (i = 0; i < m_SpawnNum; i++) {
			if ((m_spawnNumUpFlg3 && !m_spawnNumUpFlg4 && Random.Range (0, 3) == 0)
				|| (m_spawnNumUpFlg4 && !m_spawnNumUpFlg5 && Random.Range (0, 2) == 0)
				|| (m_spawnNumUpFlg5 && Random.Range (0, 2) == 0)) {
				// 四隅
				if (m_SpawnBlockPositions [i] == 0)
					m_SpawnBlockDirections [i] = Random.Range (1, 3);
				else if (m_SpawnBlockPositions [i] == m_Length - 1)
					m_SpawnBlockDirections [i] = Random.Range (2, 4);
				else if (m_SpawnBlockPositions [i] == BlockNum - 1) {
					m_SpawnBlockDirections [i] = Random.Range (0, 2);
					if (m_SpawnBlockDirections [i] == 1)
						m_SpawnBlockDirections [i] = 3;
				} else if (m_SpawnBlockPositions [i] == BlockNum - m_Length)
					m_SpawnBlockDirections [i] = Random.Range (0, 2);
				else if (0 <= m_SpawnBlockPositions [i] && m_SpawnBlockPositions [i] < m_Length)
					m_SpawnBlockDirections [i] = Random.Range (1, 4);
				else if (BlockNum - m_Length <= m_SpawnBlockPositions [i] && m_SpawnBlockPositions [i] < BlockNum) {
					m_SpawnBlockDirections [i] = Random.Range (0, 3);
					if (m_SpawnBlockDirections [i] == 2)
						m_SpawnBlockDirections [i] = 3;
				} else if (m_SpawnBlockPositions [i] % m_Length == m_Length - 1) {
					m_SpawnBlockDirections [i] = Random.Range (0, 3);
					if (m_SpawnBlockDirections [i] == 1)
						m_SpawnBlockDirections [i] = 3;
				} else if (m_SpawnBlockPositions [i] % m_Length == 0)
					m_SpawnBlockDirections [i] = Random.Range (0, 3);
			}
		}
	}
		
	// タッチした物体をtouchObjとして返す
	private bool RaycastScrollListner (Vector3 point, out GameObject touchObj)
	{
		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera> ();
		Ray ray = playercamera.ScreenPointToRay (point);
		RaycastHit hit;
		if (Physics.Raycast (ray.origin, ray.direction, out hit, (playercamera.farClipPlane - playercamera.nearClipPlane)
			, 1 << gameObject.layer)) {
			touchObj = hit.collider.gameObject;
			return true;
		}
		touchObj = null;
		return false;
	}

	private void moveBlock2 (GameObject obj, int oldpos, int newpos)
	{
		if (0 > oldpos || BlockNum <= oldpos || BlockNum <= newpos) {
			Debug.Log ("Error in moveBlock(). oldpos(" + oldpos + ")->newpos(" + newpos + ")");
		} else if (newpos >= 0) {
			m_BlockisOn [oldpos] = false;
			m_BlockisOn [newpos] = true;
			iTween.MoveTo (obj, m_Pos [newpos], m_MoveSpeed);
		} else if (newpos < 0) {
			MoveCountAdder ();
			ScoreAdder ();
			m_BlockisOn [oldpos] = false;

			switch (obj.tag) {
			case ("Right"):
				iTween.MoveTo (obj, m_Pos [(oldpos / m_Length + 1) * m_Length - 1], m_MoveSpeed);
				break;
			case ("Down"):
				iTween.MoveTo (obj, m_Pos [m_Length * (m_Length - 1) + (oldpos % m_Length)], m_MoveSpeed);
				break;
			case ("Left"):
				iTween.MoveTo (obj, m_Pos [(oldpos / m_Length) * m_Length], m_MoveSpeed);
				break;
			case ("Up"):
				iTween.MoveTo (obj, m_Pos [(oldpos % m_Length)], m_MoveSpeed);
				break;
			default:
				break;
			}
			if (MoveCount < 3) {
				obj.GetComponent<Animator> ().SetTrigger ("Move1trigger");
			} else {
				// 動いた後に false にせず itween でボム位置まで動かす
				obj.GetComponent<Animator> ().SetTrigger ("MovetoBomb1trigger");
				StartCoroutine (moveBlocktoBomb (obj));
			}
		}
	}

	// ブロックを動かす
	private IEnumerator moveBlock (GameObject obj, int oldpos, int newpos)
	{
		if (0 > oldpos || BlockNum <= oldpos || BlockNum <= newpos) {
			Debug.Log ("Error in moveBlock(). oldpos(" + oldpos + ")->newpos(" + newpos + ")");
		} else if (newpos >= 0) {
			m_BlockisOn [oldpos] = false;
			m_BlockisOn [newpos] = true;
			iTween.MoveTo (obj, m_Pos [newpos], m_MoveSpeed);
		} else if (newpos < 0) {
			MoveCountAdder ();
			ScoreAdder ();
			m_BlockisOn [oldpos] = false;

			switch (obj.tag) {
			case ("Right"):
				iTween.MoveTo (obj, m_Pos [(oldpos / m_Length + 1) * m_Length - 1], m_MoveSpeed);
				break;
			case ("Down"):
				iTween.MoveTo (obj, m_Pos [m_Length * (m_Length - 1) + (oldpos % m_Length)], m_MoveSpeed);
				break;
			case ("Left"):
				iTween.MoveTo (obj, m_Pos [(oldpos / m_Length) * m_Length], m_MoveSpeed);
				break;
			case ("Up"):
				iTween.MoveTo (obj, m_Pos [(oldpos % m_Length)], m_MoveSpeed);
				break;
			default:
				break;
			}
			if (MoveCount < 3) {
				yield return new WaitForSeconds (m_MoveSpeed);
				obj.GetComponent<Animator> ().SetTrigger ("Move1trigger");
			} else {
//				yield return new WaitForSeconds (m_MoveSpeed);
//				obj.GetComponent<Animator> ().SetTrigger ("MovetoBomb1trigger");
				StartCoroutine (moveBlocktoBomb (obj));
			}
		}
		yield break;
	}

	private IEnumerator moveBlocktoBomb (GameObject tmpobj)
	{
		if (m_BombCount < m_BombMaxNum) {
			yield return new WaitForSeconds (m_MoveSpeed);
			tmpobj.GetComponent<Animator> ().SetTrigger ("MovetoBomb1trigger");
			yield return new WaitForSeconds (0.05f);
			if (m_BombCount - 1 < m_BombMaxNum) {
				iTween.MoveTo (tmpobj, m_BombPos [m_BombCount - 1], 1.5f);
				iTween.RotateTo (tmpobj, iTween.Hash ("y", 2880, "time", 1.5f));
				iTween.FadeTo (tmpobj, iTween.Hash ("alpha", 0, "time", 0.8f));
				yield return new WaitForSeconds (0.8f);
				tmpobj.gameObject.SetActive (false);
			}
			tmpobj.gameObject.SetActive (false);
		} else {
//			Debug.Log (m_BombCount);
			tmpobj.gameObject.SetActive (false);
			yield break;
		}
	}

	// 複数のブロックを moveBlock に渡す
	private void moveBlocks (int blocknum, int count)
	{
		int i = 0;

		if (count == m_Length) {
			if (MoveCount < BombAddNum)
				m_RensaCount = 0;
		}
		if ((MoveCount >= BombAddNum && count == m_Length && GameMode == 2)
		    || (MoveCount >= BombAddNum && count == m_Length - 1 && GameMode == 1)) {
			StartCoroutine ("ManyBlocksRemove");
//			m_RensaCount++;
		}
		for (i = 0; i < blocknum; i++) {
//			if (GameMode == 1)
			StartCoroutine (moveBlock (m_blocks [i].get_obj (), m_blocks [i].get_oldpos (), m_blocks [i].get_newpos ()));
//			if (GameMode == 2)
//				moveBlock2 (m_blocks [i].get_obj (), m_blocks [i].get_oldpos (), m_blocks [i].get_newpos ());
		}
	}

	private IEnumerator swipetwice (string swiperesult)
	{
		for (int i = 1; i < m_Length; i++) {
			yield return new WaitForSeconds (m_LoopWaitTime);
			if (i == m_Length - 1)
				checkBlockNum (out m_OnBlockNum);
			swipeArrower (swiperesult, i + 1);
		}

//		yield return new WaitForSeconds (0.1f);

//		yield break;
	}

	// スワイプ時のブロックの動きを計算。moveblocks に投げる
	private void swipeArrower (string direction, int count)
	{
		int i = 0, j = 0, dir = -1, param = 0, moveblocknum = 0;
		int old_pos, new_pos;
		List<GameObject> tempobjs1;

		switch (direction) {
		case ("Right"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Right"));
			dir = 1;
			param = 1;
			break;
		case ("Down"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Down"));
			dir = 2;
			param = m_Length;
			break;
		case ("Left"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Left"));
			dir = 3;
			param = -1;
			break;
		case ("Up"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
			dir = 0;
			param = -m_Length;
			break;
		default:
			tempobjs1 = new List<GameObject> ();
			break;
		}
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x))
				    && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					break;
			}
			if (0 <= i && i < BlockNum) {
				old_pos = i;
				switch (dir) {
				// dimension x
				case (1):
				case (3):
					for (j = 0; j < m_Length; j++) {
						if (0 <= i + param && i + param < BlockNum)
						if (old_pos / m_Length == (i + param) / m_Length && !m_BlockisOn [i + param]
						    && m_BlockisOn [old_pos]) {
							i += param;
						}
					}
					new_pos = i;
					break;
				// dimension y
				case (0):
				case (2):
					for (j = 0; j < m_Length; j++) {
						if (0 <= i + param && i + param < BlockNum)
						if ((i + param) % m_Length == old_pos % m_Length && !m_BlockisOn [i + param]
						    && m_BlockisOn [old_pos]) {
							i += param;
						}
					}
					new_pos = i;
					break;
				default:
					new_pos = i;
					Debug.Log ("This is Error in swipeArrower");
					break;

				}
				if (((dir == 0 && new_pos / m_Length == 0) || (dir == 2 && new_pos / m_Length == (m_Length - 1))
				    || (dir == 1 && new_pos % m_Length == m_Length - 1) || (dir == 3 && new_pos % m_Length == 0))
				    && m_BlockisOn [old_pos]) {
					if (dir == 0 || dir == 2) {
						m_blocks [moveblocknum].set_newpos ((Mathf.Abs (new_pos - old_pos) / m_Length + 1) * -1);
					} else {
						m_blocks [moveblocknum].set_newpos ((Mathf.Abs (new_pos - old_pos) + 1) * -1);
					}
					m_blocks [moveblocknum].set_obj (touchObj);
					m_blocks [moveblocknum].set_oldpos (old_pos);
					moveblocknum++;
				} else if (old_pos != new_pos && m_BlockisOn [old_pos]) {
					m_blocks [moveblocknum].set_newpos (new_pos);
					m_blocks [moveblocknum].set_obj (touchObj);
					m_blocks [moveblocknum].set_oldpos (old_pos);
					moveblocknum++;
				}
			} else {
//				Debug.Log ("Error in swipeArrower (). Because block isn't on plate");
			}
		}
		if (moveblocknum == 0 && count == 1 && GameMode == 2) {
			StartCoroutine ("DirectionNotMatch");
		} else if (count > 0) {
			if (count == 1)
				FlickSound.Play ();
			if (count == m_Length && m_OnBlockNum == BlockNum)
			if (GameMode == 2 && !m_RefreshFlg)
				StartCoroutine ("AllBlockRemove");
			else if (GameMode == 1)
				StartCoroutine ("GameClear");
			if (count == m_Length && !m_NoBlockMoveFlg && GameMode == 2)
				StartCoroutine (appearArrowBlock (false));
			if (count == m_Length && m_NoBlockMoveFlg)
				m_NoBlockMoveFlg = false;
			
			moveBlocks (moveblocknum, count);
		} else if (count == 0) {
			if (moveblocknum != 0)
				m_FreezeBlockNum++;
		}
	}

	// 場所を引数としてそこの場所にあるブロックを返す
	private GameObject BlockGetter (int position)
	{
		List<GameObject> tempobjs = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
		tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Right"));
		tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Down"));
		tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Left"));
		foreach (GameObject obj in tempobjs) {
			if ((Mathf.RoundToInt (m_Pos [position].x) == Mathf.RoundToInt (obj.transform.position.x))
			    && (Mathf.RoundToInt (m_Pos [position].z) == Mathf.RoundToInt (obj.transform.position.z))) {
				return obj;
			}
		}
		return null;
	}

	// 上下左右に仮想的にスワイプし、もう動かせないならtrue、まだ動かせるならfalse を返す
	private bool checkEndGame ()
	{
		m_FreezeBlockNum = 0;
		swipeArrower ("Up", 0);
		swipeArrower ("Right", 0);
		swipeArrower ("Down", 0);
		swipeArrower ("Left", 0);
		if (m_FreezeBlockNum == 0 && m_OnBlockNum == 0 && GameMode == 2) {
			return true;
		} else if (m_FreezeBlockNum == 0 && m_BombCount == 0 && m_OnBlockNum != BlockNum && GameMode == 2) {
			m_FreezeBlockNum = 0;
			StartCoroutine (appearArrowBlock (false));
		} else if (m_FreezeBlockNum == 0 && m_BombCount == 0 && GameMode == 1 && m_OnBlockNum > 0) {
			return true;
		}
		m_FreezeBlockNum = 0;
		return false;
	}

	// 全消し時の動作
	private IEnumerator AllBlockRemove ()
	{
//		m_ZenkeshiImage.gameObject.SetActive (true);
		m_GameStartflg = false;
		m_EndGameflg = true;
		m_AllRemoveFlg = true;
		RefreshParticle0.Play ();
		RefreshParticle1.Play ();
		RefreshParticle2.Play ();
		RefreshParticle3.Play ();

		RemoveAllSound.Play ();
		for (int i = 0; i < 2; i++) {
			m_ScoreText.fontSize = m_ScoreFontSize + 10;
//			m_ScoreText.color = new Color ((float)255/255, (float)102/255,(float)0/255);
			Score++;
			background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
			yield return new WaitForSeconds (0.1f);
			m_ScoreText.fontSize = m_ScoreFontSize;
//			background.GetComponent<Renderer> ().material.color = backgroundColor;
			yield return new WaitForSeconds (0.1f);
		}
		background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
		m_ScoreText.fontSize = m_ScoreFontSize + 10;
		m_AllRemoveFlg = false;
		m_EndGameflg = false;
		m_GameStartflg = true;
		yield return new WaitForSeconds (0.7f);
		background.GetComponent<Renderer> ().material.color = backgroundColor;
		m_ScoreText.fontSize = m_ScoreFontSize;
		m_ScoreText.color = m_ScoreColor;
	}

	// ボム使用時の動作
	private void BombAction (GameObject touchObj)
	{
		int i;

		if (m_BombCount > 0) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x))
				    && (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					break;
			}
			m_BlockisOn [i] = false;
			BombDowner ();
//			touchObj.GetComponent<Animator> ().SetTrigger ("Bombtrigger");
			BombParticle.gameObject.transform.position = m_BombPos[m_BombCount];
//			BombParticle.gameObject.transform.position = 
//				Vector3.right * touchObj.gameObject.transform.position.x +
//				Vector3.up * (touchObj.gameObject.transform.position.y + 1.0f) +
//				Vector3.forward * touchObj.gameObject.transform.position.z;

//			switch (touchObj.tag) {
//			case "Up":
//				BombParticle.startColor = new Color ((float)153 / 255, (float)204 / 255, (float)238 / 255);
//				break;
//			case "Right":
//				BombParticle.startColor = new Color ((float)221 / 255, (float)153 / 255, (float)153 / 255);
//				break;
//			case "Down":
//				BombParticle.startColor = new Color ((float)221 / 255, (float)221 / 255, (float)68 / 255);
//				break;
//			case "Left":
//				BombParticle.startColor = new Color ((float)136 / 255, (float)221 / 255, (float)170 / 255);
//				break;
//			default:
//				BombParticle.startColor = Color.white;
//				break;
//			}
//			BombParticle.Play ();
			touchObj.gameObject.SetActive (false);
			checkBlockNum (out m_OnBlockNum);
			if (m_OnBlockNum == BlockNum)
				StartCoroutine (appearArrowBlock (false));
		} else if (m_BombCount == 0) {
			NoBombSound.Play ();
		}
	}
		
	// スワイプした方向を返す
	private void swipeDirectionChecker (Vector3 startPos, Vector3 stopPos, out string swipeResult)
	{
		float moveX = stopPos.x - startPos.x;
		float moveY = stopPos.y - startPos.y;

		if (moveX > 50 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Right";
		} else if (moveX < -50 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Left";
		} else if (moveY > 50 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Up";
		} else if (moveY < -50 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Down";
		} else {
			swipeResult = null;
		}
	}

	// ブロックの出現　＆＆　透明ブロックの出現
	private IEnumerator appearArrowBlock (bool timeupflg)
	{
		int i = 0;

		if (!timeupflg) {
			if (!m_AllRemoveFlg)
				yield return new WaitForSeconds (0.1f);
			else
				yield return new WaitForSeconds (0.4f);
		}
		// 透明ブロック非表示
		foreach (GameObject block in UnityEngine.GameObject.FindGameObjectsWithTag ("Clear")) {
			block.SetActive (false);
		}
		// ブロック出現
		if (!m_RefreshFlg)
			for (i = 0; i < m_SpawnNum; i++) {
				if (m_SpawnBlockPositions [i] >= 0) {
					if (m_BlockisOn [m_SpawnBlockPositions [i]] == false) {
						m_BlockisOn [m_SpawnBlockPositions [i]] = true;
						spawnBlockPos (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
					} else
						Debug.Log ("のせたいところにブロックが既にあり spawn できない");
				}
			}

		if (GameMode == 2) {
			if (!m_spawnNumUpFlg1) {
				if (m_spawnNumUpFlg0) {
					int seed = Random.Range (0, 2);
					if (seed == 0) {
						m_SpawnNum = m_Length - 2;
					} else if (seed == 1) {
						m_SpawnNum = m_Length - 1;
					}
				}

			} else if (m_spawnNumUpFlg1 && !m_spawnNumUpFlg2) {
				// 100 ~ 200
				m_SpawnNum = m_Length - 1;

			} else if (m_spawnNumUpFlg2 && !m_spawnNumUpFlg2_1) {
				// 200 ~ 250
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = m_Length - 2;
				else if (seed == 1 || seed == 2)
					m_SpawnNum = m_Length - 1;
				else if (seed == 3 || seed == 4)
					m_SpawnNum = m_Length;

			} else if (m_spawnNumUpFlg2_1 && !m_spawnNumUpFlg3_1) {
				// 250 ~ 300
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = m_Length - 1;
				else if (seed == 1 || seed == 2 || seed == 3)
					m_SpawnNum = m_Length;
				else if (seed == 4)
					m_SpawnNum = m_Length + 1;

			} else if (m_spawnNumUpFlg3_1) {
				// 300 ~ 400
				int seed = Random.Range (0, 5);
				if (seed == 0 || seed == 1 || seed == 2)
					m_SpawnNum = m_Length;
				else if (seed == 3 || seed == 4)
					m_SpawnNum = m_Length + 1;

			} else if (m_spawnNumUpFlg4) {
				// 400 ~ 500
				int seed = Random.Range (0, 5);
				if (seed == 0 || seed == 1)
					m_SpawnNum = m_Length;
				else if (seed == 2 || seed == 3 || seed == 4)
					m_SpawnNum = m_Length + 1;

			} else if (m_spawnNumUpFlg5) {
				// 500 ~ 
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = m_Length;
				else if (seed == 1 || seed == 2 || seed == 3 || seed == 4)
					m_SpawnNum = m_Length + 1;
			}
		}
		// 次の透明ブロックの表示
		spawnPosSetter ();
		for (i = 0; i < m_SpawnNum; i++) {
			if (m_SpawnBlockPositions [i] >= 0 && GameMode == 2)
				spawnClearBlock (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
		}
		checkBlockNum (out m_OnBlockNum);
		yield break;
	}

	// スワイプ方向にブロックが無い場合の処理
	private IEnumerator DirectionNotMatch ()
	{
		m_NoBlockMoveFlg = true;
		background.GetComponent<Renderer> ().material.color = Color.yellow;
		StartCoroutine (appearArrowBlock (false));
		yield return new WaitForSeconds (0.1f);
		if (background.GetComponent<Renderer> ().material.color != Color.gray)
			background.GetComponent<Renderer> ().material.color = backgroundColor;
		yield break;
	}

	// ステージ生成
	private void stageSet ()
	{
		int i;

		Score = 0;
		NowTime = 0.0f;
		m_BombCounter = 0;
		m_Length = (int)Mathf.Sqrt (BlockNum);
		m_SpawnNum = m_Length - 2;
		if (GameMode == 1) {
			m_SpawnNum = m_Length;
			for (i = 0; i < m_Length - 2; i++)
				StartCoroutine (appearArrowBlock (false));
			m_BombMaxNum = m_Length - 2;
			m_BombCount = m_BombMaxNum / 2 + m_BombMaxNum % 2;
		}
		m_BlockisOn = new bool[BlockNum];
		m_Pos = new Vector3[BlockNum];
		m_BombPos = new Vector3[m_BombMaxNum];
		m_GagePos = new Vector3[BombUpSpan];
		m_BombObj = new GameObject[m_BombMaxNum];
		m_SpawnBlockPositions = new int[BlockNum];
		m_tmpPosition1 = new int[BlockNum];
		m_tmpPosition2 = new int[BlockNum];
		m_tmpPosition3 = new int[BlockNum];
		m_SpawnBlockDirections = new int[BlockNum];
		m_AllRemoveColor = new Color ((float)246 / 255, (float)240 / 255, (float)195 / 255, (float)192 / 255);
		originalBackGroundColor = new Color ((float)181 / 255, (float)220 / 255, (float)231 / 255, (float)192 / 255);
//		m_AllRemoveColor = new Color ((float)40 / 255, (float)242 / 255, (float)224 / 255, (float)192 / 255);

		AudioSource[] audioSources = GetComponents<AudioSource> ();
		FlickSound = audioSources [0];
		BombSound = audioSources [1];
		BombUpSound = audioSources [2];
		LevelUpSound = audioSources [3];
		NoBombSound = audioSources [4];
		RemoveAllSound = audioSources [5];
		FlickSound.clip = AudioFlick;
		BombSound.clip = AudioBomb;
		BombUpSound.clip = AudioBombUp;
		LevelUpSound.clip = AudioLevelUp;
		NoBombSound.clip = AudioNoBomb;
		RemoveAllSound.clip = AudioRemoveAll;

		m_blocks = new BlockInfo[BlockNum];
		background = GameObject.Find ("BackGround");
		backgroundColor = background.GetComponent<Renderer> ().sharedMaterial.color;
		background.GetComponent<Renderer> ().material.color = backgroundColor;
		RawImages = canvas.GetComponentsInChildren<RawImage> ();

		foreach (RawImage raw in RawImages) {
			if (raw.name == "AllBlockRemove")
				m_ZenkeshiImage = raw;
			if (raw.name == "Good")
				m_GoodImage = raw;
			if (raw.name == "Excellent")
				m_ExcellentImage = raw;
			if (raw.name == "Great")
				m_GreatImage = raw;
			if (raw.name == "Scored")
				m_ScoredImage = raw;
			if (raw.name == "Star" && GameMode == 1)
				raw.gameObject.SetActive (false);
		}
//		EndMenu.gameObject.SetActive (false);
		foreach (Text text in canvas.GetComponentsInChildren<Text>()) {
			if (text.name == "Score") {
				m_ScoreText = text;
				m_ScoreColor = m_ScoreText.color;
				m_originalScoreColor = m_ScoreColor;
				m_ScoreFontSize = text.fontSize;
			}
//			if (text.name == "MaxScore")
//				m_MaxScoreText = text;
			if (text.name == "Rensa")
				m_RensaText = text;
			if (text.name == "Time")
				m_TimeText = text;
			if (text.name == "MaxTime")
				m_MaxTimeText = text;
			if (text.name == "Bonus")
				m_BonusText = text;
			if (text.name == "BonusScore")
				m_BonusScoreText = text;
		}
		m_RensaText.gameObject.SetActive (false);
		m_ZenkeshiImage.gameObject.SetActive (false);
		m_GoodImage.gameObject.SetActive (false);
		m_ExcellentImage.gameObject.SetActive (false);
		m_GreatImage.gameObject.SetActive (false);
		m_ScoredImage.gameObject.SetActive (false);
		if (GameMode == 1) {
			m_ScoreText.gameObject.SetActive (false);
//			m_MaxScoreText.gameObject.SetActive (false);
			m_BonusText.gameObject.SetActive (false);
			m_BonusScoreText.gameObject.SetActive (false);
			m_MaxTimeText.text = "" + (int)MaxTime[m_Length];
		} else if (GameMode == 2) {
			m_TimeText.gameObject.SetActive (false);
			m_MaxTimeText.gameObject.SetActive (false);
		}

		// 初期化
		for (i = 0; i < BlockNum; i++) {
			m_BlockisOn [i] = false;
			m_Pos [i] = new Vector3 (0f, 0f, 0f);
			if (i < m_BombMaxNum)
				m_BombPos [i] = new Vector3 (0f, 0f, 0f);
			m_SpawnBlockPositions [i] = 0;
			m_tmpPosition1 [i] = 0;
			m_tmpPosition2 [i] = 0;
			m_tmpPosition3 [i] = 0;
			m_blocks [i].set_obj (null);
			m_blocks [i].set_oldpos (-1);
			m_blocks [i].set_newpos (-1);
			m_SpawnBlockDirections [i] = 0;
		}
		for (i = 0; i < BombUpSpan; i++)
			m_GagePos [i] = new Vector3 (0f, 0f, 0f);
			
		m_Pos [0] = Vector3.right * (this.transform.position.x)
		+ Vector3.up * this.transform.position.y
		+ Vector3.forward * (this.transform.position.z);
		for (i = 1; i < BlockNum; i++) {
			m_Pos [i] = Vector3.right * (m_Pos [i - 1].x + 1.0f) + Vector3.up * m_Pos [i - 1].y
			+ Vector3.forward * m_Pos [i - 1].z;
			if (i % (int)Mathf.Sqrt ((float)BlockNum) == 0)
				m_Pos [i] = Vector3.right * (m_Pos [i - 1].x - ((int)Mathf.Sqrt ((float)BlockNum) - 1) * 1.0f)
				+ Vector3.up * m_Pos [i - 1].y + Vector3.forward * (m_Pos [i - 1].z - 1.0f);
		}
		if (m_BombMaxNum != 2)
			for (i = 0; i < m_BombMaxNum; i++)
				m_BombPos [i] = Vector3.right * m_Pos [m_Length - 1].x / (m_BombMaxNum - 1) * i
				+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [m_Length - 1].z / (m_BombMaxNum - 1) * i + 1.2f + (m_Length - 5) * 0.1f);
		else if (m_BombMaxNum == 2)
			for (i = 0; i < m_BombMaxNum; i++)
				m_BombPos [i] = Vector3.right * m_Pos [i].x
				+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [i].z + 1.3f);

		for (i = 0; i < BombUpSpan; i++)
			m_GagePos [i] = Vector3.right * m_Pos [m_Length - 1].x / (BombUpSpan - 1) * i
			+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [m_Length - 1].z / (BombUpSpan - 1) * i - 4.0f);

		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera> ();
		playercamera.transform.position = Vector3.right * (m_Pos [0].x + (float)(m_Length - 1) / 2)
		+ Vector3.up * playercamera.transform.position.y
		+ Vector3.forward * (m_Pos [0].z - (1.1f + ((int)m_Length - 4) * 0.5f));
		if (GameMode == 2)
			playercamera.orthographicSize = m_Length;
		else if (GameMode == 1)
			playercamera.orthographicSize = m_Length - 1.0f;
		for (i = 0; i < BlockNum; i++) {
			m_UnderPlate = (GameObject)Instantiate (UnderPlate);
			m_UnderPlate.tag = "UnderPlate";
			m_UnderPlate.transform.position = m_Pos [i] + Vector3.up * (m_Pos [i].y - 0.1f);
		}
		BombSet ();
		spawnPosSetter ();
		for (i = 0; i < m_SpawnNum; i++)
			m_SpawnBlockDirections [i] = Random.Range (0, 4);
		for (i = 0; i < m_BombCount; i++)
			BombSpawn (i + 1, 1);
		if (GameMode == 2)
			StartCoroutine (appearArrowBlock (false));
		else if (GameMode == 1)
			// 修正　あやしい
			for (i = 0; i < BlockNum; i++)
				if (!m_BlockisOn [i]) {
					GameObject obj = BlockGetter (i);
					if (obj) {
						obj.gameObject.SetActive (false);
						Debug.Log (i);
					}
				}
	}

	private IEnumerator BombUpAction (GameObject target)
	{
		if (m_BombCount < m_BombMaxNum) {
			iTween.MoveTo (target, m_BombPos [m_BombCount], 1.5f);
			yield return new WaitForSeconds (1.5f);
			target.gameObject.SetActive (false);
		} else {
			Vector3 tmppos = Vector3.right * (m_BombPos [2].x - 0.5f)
			                 + Vector3.up * m_BombPos [2].y + Vector3.forward * (m_BombPos [2].z + 0.6f);
			iTween.MoveTo (target, tmppos, 0.7f);
			m_ScoreText.fontSize = m_ScoreFontSize + 10;
			m_ScoreText.color = new Color ((float)255 / 255, (float)102 / 255, (float)0 / 255);
			yield return new WaitForSeconds (0.3f);
			Score = Score + 2;
			yield return new WaitForSeconds (0.4f);
			m_ScoreText.fontSize = m_ScoreFontSize;
			m_ScoreText.color = m_ScoreColor;
			target.gameObject.SetActive (false);

		}
		yield break;
	}

	// uGUI用
	private void uguiSet ()
	{
		if (GameMode == 1) {
			m_TimeText.text = "" + (int)NowTime;
			m_MaxTimeText.text = "" + (int)MaxTime [m_Length];
		} else if (GameMode == 2) {
			m_ScoreText.text = "" + Score;
//			m_MaxScoreText.text = "" + MaxScore;
			m_BonusScoreText.text = "" + (Score / 100 + 1) * 100;
		}
		m_ScoreText.color = m_ScoreColor;
//		m_MaxScoreText.color = m_ScoreColor;
//		m_MaxScoreText2.color = m_ScoreColor;
		m_BonusText.color = m_ScoreColor;
		m_BonusScoreText.color = m_ScoreColor;
		if (Score > MaxScore && !maxScoreUpdate && MaxScore != 0 && GameMode == 2) {
			maxScoreUpdate = true;
			StartCoroutine ("scoreUpdated");	
		}
	}

	// ブロックの Instantiate
	private void spawnClearBlock (int direction, int position)
	{
		switch (direction) {
		case 0:
			m_ClearBlock = (GameObject)Instantiate (ClearBlue);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 1:
			m_ClearBlock = (GameObject)Instantiate (ClearRed);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 2:
			m_ClearBlock = (GameObject)Instantiate (ClearYellow);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 3:
			m_ClearBlock = (GameObject)Instantiate (ClearGreen);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 90f;
			break;
		default:
			Debug.Log ("Error in spawnClearBlock");
			break;
		}
		m_ClearBlock.tag = "Clear";
		m_ClearBlock.transform.position = Vector3.right * m_Pos [position].x +
		Vector3.up * (m_Pos [position].y - 0.01f) + Vector3.forward * m_Pos [position].z;
	}

	private void spawnBlockPos (int direction, int position)
	{
		switch (direction) {
		case 0:
			m_Block = (GameObject)Instantiate (BlueBlock);
			m_Block.tag = "Up";
			m_Block.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 1:
			m_Block = (GameObject)Instantiate (RedBlock);
			m_Block.tag = "Right";
			m_Block.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 2:
			m_Block = (GameObject)Instantiate (YelloBlock);
			m_Block.tag = "Down";
			m_Block.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 3:
			m_Block = (GameObject)Instantiate (GreenBlock);
			m_Block.tag = "Left";
			m_Block.transform.eulerAngles = Vector3.up * 90f;
			break;
		case 4:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "UpBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 5:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "RightBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 6:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "DownBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 7:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "LeftBlack";
			break;
		default:
			Debug.Log ("Error in spawnBlockPos");
			break;
		}
		if (0 <= direction && direction < 4)
			m_Block.transform.position = m_Pos [position];
		else if (4 <= direction && direction < 8)
			m_DownBlackBlock.transform.position = m_Pos [position];
	}
	// ボムの初期化
	private void BombSet ()
	{
		for (int i = 0; i < m_BombMaxNum; i++) {
			m_BombObj [i] = (GameObject)Instantiate (Bomb);
			m_BombObj [i].transform.position = m_BombPos [i];
			m_BombObj [i].SetActive (false);
			m_BombSpaceObj = (GameObject)Instantiate (BombSpace);
			m_BombSpaceObj.transform.position = m_BombPos [i];
		}
	}

	private IEnumerator ManyBlocksRemove ()
	{
		BombAdder ();

		for (int i = 0; i < MoveCount / 3 - 1; i++)
			BombAdder ();

//		Debug.Log (m_RensaCount);
//		if (m_RensaCount > 1) {
//			m_RensaText.gameObject.SetActive (true);
//			m_RensaText.text = m_RensaCount + "連鎖";
//		} else 
		if (GameMode == 2) {
			if (!m_AllRemoveFlg && MoveCount == 3) {
//			m_RensaText.gameObject.SetActive (false);
//			m_GoodImage.gameObject.SetActive (true);
			} else {
//			m_RensaText.gameObject.SetActive (false);
				m_ScoreText.fontSize = m_ScoreFontSize + 10;
				m_ScoreText.color = new Color ((float)255 / 255, (float)102 / 255, (float)0 / 255);
//				if (!m_AllRemoveFlg && (4 <= MoveCount && MoveCount < 5))
//					m_GoodImage.gameObject.SetActive (true);
//				if (!m_AllRemoveFlg && (5 <= MoveCount && MoveCount < BlockNum / 2))
//					m_GreatImage.gameObject.SetActive (true);
//				if (!m_AllRemoveFlg && (BlockNum / 2 <= MoveCount))
//					m_ExcellentImage.gameObject.SetActive (true);
				background.GetComponent <Renderer> ().material.color = m_AllRemoveColor;
				for (int i = 3; i < MoveCount; i++) {
					Score = Score + 2;
				}
				yield return new WaitForSeconds (0.5f);
				m_ScoreText.fontSize = m_ScoreFontSize;
				m_ScoreText.color = m_ScoreColor;
				background.GetComponent <Renderer> ().material.color = backgroundColor;
			}
		}
		yield break;
	}

	private IEnumerator LevelUp ()
	{
		NowLevel++;
		LevelUpSound.Play ();
		yield break;
	}

	// ボムの Instantiate
	private void BombSpawn (int num, int flg)
	{
		// 2 -> BombDowner, 1 -> BombAdder
		if (flg == 1) {
			if (m_GameStartflg) {
				BombUpSound.Play ();
			}
			m_BombObj [num - 1].SetActive (true);
			m_BombObj [num - 1].GetComponent <Animator> ().SetTrigger ("BombPopupTrigger");
		}
		if (flg == 2) {
			if (m_GameStartflg)
				BombSound.Play ();
			//LevelUpSound.Play();
			m_BombObj [num].GetComponent<Animator> ().SetTrigger ("BombRemoveTrigger");
		}

	}

	private IEnumerator RefreshEfectStart ()
	{
		RefreshParticle0.Play ();
		RefreshParticle1.Play ();
		RefreshParticle2.Play ();
		RefreshParticle3.Play ();

		yield break;
	}

	// ゴール達成時の挙動
	private IEnumerator RefreshStage ()
	{
//		m_ExcellentImage.gameObject.SetActive (true);
		if (!m_AllRemoveFlg) {
			LevelUpSound.Play ();
			m_GameStartflg = false;
			m_EndGameflg = true;
			m_RefreshFlg = true;
			StartCoroutine ("RefreshEfectStart");
			yield return new WaitForSeconds (m_MoveSpeed);

			List<GameObject> tempobjs = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
			tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Right"));
			tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Down"));
			tempobjs.AddRange (GameObject.FindGameObjectsWithTag ("Left"));
			foreach (GameObject obj in tempobjs) {
				obj.GetComponent<Animator> ().SetTrigger ("Bombtrigger");
			}
			for (int i = 0; i < BlockNum; i++) {
				m_BlockisOn [i] = false;
			}
			yield return new WaitForSeconds (0.8f);
		}
		CutInMenu.gameObject.SetActive (true);
		foreach (Text tex in CutInMenu.GetComponentsInChildren<Text> ()) {
			if (tex.name == "GoalScore")
				tex.text = "" + (Score / 100 + 1) * 100;
			if (tex.name == "OldGoalScore")
				tex.text = "" + Score;
		}
		yield return new WaitForSeconds (1.6f);
		CutInMenu.gameObject.SetActive (false);
		m_GameStartflg = true;
		m_EndGameflg = false;
		m_RefreshFlg = false;
		StartCoroutine (appearArrowBlock (false));
		if (originalBackGroundColor.b > (float)230/255)
			originalBackGroundColor = new Color (originalBackGroundColor.r, originalBackGroundColor.g, originalBackGroundColor.b - (float)30 / 255);
		else if (originalBackGroundColor.r <  (float)210/255)
			originalBackGroundColor = new Color (originalBackGroundColor.r + (float)20 / 255, originalBackGroundColor.g, originalBackGroundColor.b);
		else if (originalBackGroundColor.g < (float)240/255)
			originalBackGroundColor = new Color (originalBackGroundColor.r + (float)10 / 255, originalBackGroundColor.g + (float)10 / 255, originalBackGroundColor.b + (float)20 / 255);
		
		backgroundColor = originalBackGroundColor;
//		BlockNum = (m_Length + 1) * (m_Length + 1);
//		Application.LoadLevel ("Main");

		yield break;
	}

	private void BombAdder ()
	{
		if (m_BombCount < m_BombMaxNum) {
			m_BombCount++;
			BombSpawn (m_BombCount, 1);
		}
	}

	private void BombDowner ()
	{
		m_BombCount--;
		BombSpawn (m_BombCount, 2);
	}

	public static void MoveCountAdder ()
	{
		MoveCount++;
	}

	// スコア更新
	public static void ScoreAdder ()
	{
		Score++;
		m_BombCounter++;
	}

	// 最大スコア更新
	public static void MaxScoreUpdater ()
	{
		MaxScore = Score;
	}

	public static void MaxTimeUpDater ()
	{
		MaxTime [(int)Mathf.Sqrt (BlockNum)] = NowTime;
		Debug.Log ((int)Mathf.Sqrt (BlockNum));
	}

	// スコアを減らす
	public static void ScoreDowner ()
	{
		Score--;
	}

	public void title ()
	{
		if (MaxScore < Score) {
			MaxScoreUpdater ();
		}
		Application.LoadLevel ("Title");
	}

	// クリア失敗
	private IEnumerator FailGame ()
	{
		m_GameStartflg = false;
		m_EndGameflg = true;
		background.GetComponent<Renderer> ().material.color = Color.gray;
		yield return new WaitForSeconds (2f);
		Application.LoadLevel ("Main");
		yield break;
	}

	public void reset ()
	{
		if (MaxScore < Score) {
			MaxScoreUpdater ();
		}
		Application.LoadLevel ("Main");
	}

	private IEnumerator GameClear ()
	{
		m_GameStartflg = false;
		m_EndGameflg = true;
		background.GetComponent<Renderer> ().material.color = Color.yellow;
		yield return new WaitForSeconds (2f);
		Application.LoadLevel ("End");
		yield break;
	}

	private IEnumerator EndGamePopup ()
	{
		m_GameStartflg = false;
		m_EndGameflg = true;

		background.GetComponent<Renderer> ().material.color = Color.gray;
		yield return new WaitForSeconds (1f);
		EndMenu.SetActive (true);
		foreach (Text tex in EndMenu.GetComponentsInChildren<Text> ()) {
			if (tex.name == "EndScore" && GameMode == 2)
				tex.text = "" + Score;
			if (tex.name == "EndScore" && GameMode == 1)
				tex.text = "" + (int)NowTime;
			if (tex.name == "GameOverText" && GameMode == 1)
				tex.text = "Game Clear !!";
			if (tex.name == "GameOverText" && GameMode == 2 && MaxScore < Score)
				tex.text = "スコア更新 !";
			if (tex.name == "HighScore" && GameMode == 2)
				tex.text = "" + MaxScore;
		}
	}

	// ゲームを終わらせる
	private IEnumerator EndGame ()
	{
		m_GameStartflg = false;
		m_EndGameflg = true;
		background.GetComponent<Renderer> ().material.color = Color.gray;
		yield return new WaitForSeconds (2f);
		Application.LoadLevel ("End");
		yield break;
	}

	private IEnumerator scoreUpdated ()
	{
		m_ScoredImage.gameObject.SetActive (true);

		LevelUpSound.Play ();
		yield break;
	}
}