﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	[SerializeField]
	private GameObject m_Child;
	public GameObject Child {
		get{ return m_Child; }
	}

	public void SetAllTag(string setTag)
	{
		//this.tag = setTag;
		m_Child.tag = setTag;
	}
}
