﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class OldGameController : MonoBehaviour
{
	struct BlockInfo {
		private GameObject obj;
		private int oldpos;
		private int newpos;

		public void set_obj (GameObject var) {
			obj = var;
		}
		public void set_oldpos (int var) {
			oldpos = var;
		}
		public void set_newpos (int var) {
			newpos = var;
		}
		public GameObject get_obj () {
			return obj;
		}
		public int get_oldpos () {
			return oldpos;
		}
		public int get_newpos () {
			return newpos;
		}
	}

	// GameMode  1 -> テスト、2 -> 玄人向け、4 -> ノーマル
	public static int GameMode = 4;
	public static int BlockNum = 16;
	private BlockInfo[] m_blocks;
	private int m_Length;
	private int m_BombMaxNum = 4;
	public GameObject BlueBlock, RedBlock, YelloBlock, GreenBlock, BlackBlock, ClearBlock, ClearBlue, ClearRed, ClearYellow, ClearGreen;
	public GameObject UnderPlate;
	public GameObject Bomb, BombSpace;
	private Vector3[] m_Pos, m_BombPos, m_GagePos;
	private GameObject m_Block, m_DownBlackBlock, m_ClearBlock;
	private GameObject m_UnderPlate, m_BombSpaceObj;
	private GameObject[] m_BombObj;
	public static Color originalcolor;
	public ParticleSystem BombParticle;
	private GameObject background;
	public AudioClip AudioFlick, AudioBomb, AudioBombUp, AudioLevelUp, AudioNoBomb, AudioRemoveAll;
	AudioSource FlickSound, BombSound, BombUpSound, LevelUpSound, NoBombSound, RemoveAllSound;
	public static int m_BombCounter = 0;
	public Canvas canvas;
	public static int Score = 0;
	public static int NowLevel = 0;
	public static int MaxScore = 0;
	public static int UserLevel = 0;
	private int m_FreezeBlockNum = 0;
	private int m_RensaCount = 0;

	private float m_MoveSpeed = 0.05f;
	private float m_LeftTime;
	public float LimitTime = 5.0f;
	private float m_LoopWaitTime = 0.01f;

	private bool[] m_BlockisOn;
	private bool m_SwipeStart = false;
	private string m_SwipeResult = null;
	private Vector3 m_SwipeStartPos, m_SwipeStopPos;
	private GameObject m_TouchObj;
	private int[] m_SpawnBlockPositions, m_tmpPosition1, m_tmpPosition2, m_tmpPosition3;
	private int[] m_SpawnBlockDirections;

	private bool m_GameStartflg = false, m_EndGameflg = false;
	private int m_SpawnNum = 2;
	private bool m_spawnNumUpFlg0 = false, m_spawnNumUpFlg1 = false, m_spawnNumUpFlg2 = false
		, m_spawnNumUpFlg2_1 = false, m_spawnNumUpFlg3 = false,m_spawnNumUpFlg3_1 = false, m_spawnNumUpFlg4 = false
		, m_spawnNumUpFlg5 = false, m_spawnNumUpFlg6 = false, m_spawnNumUpFlg7 = false, m_spawnNumUpFlg8 = false
		, m_spawnNumUpFlg9 = false;
	private int m_BombCount = 1;
	public int BombUpSpan = 10;
	public static int BombAddNum = 3;
	public static int MoveCount = 0;
	private bool m_TimeStopFlg = false;
	private int m_OnBlockNum;
	private bool m_AllRemoveFlg = false;
	private Slider[] slider ;
	private Color m_AllRemoveColor;
	private RawImage[] RawImages;
	private RawImage m_ZenkeshiImage, m_GoodImage, m_ExcellentImage, m_GreatImage;
	private Text m_ScoreText, m_MaxScoreText, m_LevelText, m_RensaText;
	private bool m_NoBlockMoveFlg = false;

	void Start ()
	{
		stageSet ();
	}

	void Update ()
	{
		uguiSet ();
		ScoreChecker ();
//		checkBlockNum (out m_OnBlockNum);
		if (!m_GameStartflg)
			if (Input.GetMouseButtonDown (0) && !m_EndGameflg)
				m_GameStartflg = true;

		if (m_GameStartflg) {
			if (!m_TimeStopFlg) {
				m_LeftTime -= Time.deltaTime;
			}
			m_SwipeResult = null;

			// スワイプの検知
			if (Input.GetMouseButtonDown (0)) {
				MoveCount = 0;
				m_SwipeStartPos = Input.mousePosition;
				m_SwipeStart = true;
				RaycastScrollListner (Input.mousePosition, out m_TouchObj);
			} else if (Input.GetMouseButton (0) && m_SwipeStart) {
				m_SwipeStopPos = Input.mousePosition;
				swipeDirectionChecker (m_SwipeStartPos, m_SwipeStopPos, out m_SwipeResult);
				if (m_SwipeResult != null && m_OnBlockNum != BlockNum) {
					swipeArrower (m_SwipeResult, 1);
					StartCoroutine (swipetwice(m_SwipeResult));
					m_SwipeResult = null;
					m_SwipeStart = false;
				}
			} else if (Input.GetMouseButtonUp (0) 
				&& (m_TouchObj.name == "BlueBlock(Clone)" || m_TouchObj.name == "YellowBlock(Clone)" ||
				m_TouchObj.name == "GreenBlock(Clone)" || m_TouchObj.name == "RedBlock(Clone)") && m_SwipeStart) {
				BombAction (m_TouchObj);
				m_SwipeStart = false;
			}
		}
	}

	// 終了判定
	private void checkBlockNum (out int checker) {
		checker = 0;
		foreach (bool check in m_BlockisOn) {
			if (!check) {
				checker++;
			}
		}
		if (checker == 0) {
			if (m_LeftTime <= 0)
				StartCoroutine ("EndGame");
			if (checkEndGame ())
				StartCoroutine ("EndGame");
		} else if (0 < checker && checker < (int)BlockNum / 4) {
			originalcolor = new Color ((float)204 / 255, (float)140 / 255, (float)140 / 255, originalcolor.a);
			if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				|| background.GetComponent<Renderer> ().material.color != Color.yellow
				|| background.GetComponent<Renderer> ().material.color != Color.gray)
				background.GetComponent<Renderer> ().material.color = originalcolor;
		} else if ((int)BlockNum / 4 < checker && checker < (int)BlockNum / 2) {
			originalcolor = new Color ((float)204 / 255, (float)190 / 255, (float)190 / 255);
			if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				|| background.GetComponent<Renderer> ().material.color != Color.yellow)
				background.GetComponent<Renderer> ().material.color = originalcolor;
		} else if ((int)BlockNum / 2 < checker && checker < BlockNum) {
			originalcolor = new Color (0.784f, 0.898f, 1.0f, originalcolor.a);
			if (background.GetComponent<Renderer> ().material.color != m_AllRemoveColor
				|| background.GetComponent<Renderer> ().material.color != Color.yellow)
				background.GetComponent<Renderer> ().material.color = originalcolor;
		}
	}

	// スコアごとの処理
	private void ScoreChecker () {
		if (Score > 50 && !m_spawnNumUpFlg0) {
			m_spawnNumUpFlg0 = true;
			m_SpawnNum++;
//			originalcolor = new Color (originalcolor.r - (float)80 / 255, originalcolor.g, 
//				originalcolor.b, originalcolor.a);
//			background.GetComponent<Renderer> ().material.color = originalcolor;
		}
		if (Score > 100 && !m_spawnNumUpFlg1) {
			m_spawnNumUpFlg1 = true;
//			originalcolor = new Color (originalcolor.r, originalcolor.g - (float)20 / 255, 
//				originalcolor.b - (float)0 / 255, originalcolor.a);
//			background.GetComponent<Renderer> ().material.color = originalcolor;
			StartCoroutine ("LevelUp");
		}
		if (Score > 200 && !m_spawnNumUpFlg2) {
			m_SpawnNum++;
			m_spawnNumUpFlg2 = true;
//			originalcolor = new Color (originalcolor.r, originalcolor.g - (float)20 / 255, 
//				originalcolor.b - (float)40 / 255, originalcolor.a);
//			background.GetComponent<Renderer> ().material.color = originalcolor;
			StartCoroutine ("LevelUp");
		}
		if (Score > 250 && !m_spawnNumUpFlg2_1) {
			m_spawnNumUpFlg2_1 = true;
//			originalcolor = new Color ((float)204 / 255, (float)190 / 255, (float)190 / 255);
//			background.GetComponent<Renderer> ().material.color = originalcolor;
		}
		if (Score > 300 && !m_spawnNumUpFlg3) {
			m_spawnNumUpFlg3 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 350 && !m_spawnNumUpFlg3_1) {
			m_spawnNumUpFlg3_1 = true;
//			originalcolor = new Color ((float)204 / 255, (float)102 / 255, (float)102 / 255);
//			background.GetComponent<Renderer> ().material.color = originalcolor;
			//StartCoroutine ("LevelUp");
		}
		if (Score > 400 && !m_spawnNumUpFlg4) {
			m_spawnNumUpFlg4 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 500 && !m_spawnNumUpFlg5) {
			m_spawnNumUpFlg5 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 600 && !m_spawnNumUpFlg6) {
			m_spawnNumUpFlg6 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 700 && !m_spawnNumUpFlg7) {
			m_spawnNumUpFlg7 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 800 && !m_spawnNumUpFlg8) {
			m_spawnNumUpFlg8 = true;
			StartCoroutine ("LevelUp");
		}
		if (Score > 900 && !m_spawnNumUpFlg9) {
			m_spawnNumUpFlg9 = true;
			StartCoroutine ("LevelUp");
		}
		if (m_BombCounter / BombUpSpan > 0) {
			m_BombCounter -= BombUpSpan;
			BombAdder ();
			if (BombUpSpan == 10) {
				// 50 ~ 100
				if (LimitTime > 4f && m_spawnNumUpFlg0) {
					LimitTime -= 0.2f;
					// 100 ~ 200
				} else if (LimitTime > 2.5f && m_spawnNumUpFlg1 && !m_spawnNumUpFlg2) {
					LimitTime -= 0.15f;
					//200 ~ 250
				} else if (LimitTime < 3.0f && m_spawnNumUpFlg2 && !m_spawnNumUpFlg2_1) {
					LimitTime += 0.1f;
//				} else if (LimitTime > 2.0f && m_spawnNumUpFlg3) {
//					LimitTime -= 0.1f;
					// 250 ~ 350
				} else if (LimitTime > 2.3f && m_spawnNumUpFlg2_1) {
					LimitTime -= 0.1f;
					// 350  ~
				} else if (LimitTime > 1.5f && m_spawnNumUpFlg3_1) {
					LimitTime -= 0.1f;
				} else if (LimitTime > 1.0f && m_spawnNumUpFlg4) {
					LimitTime -= 0.05f;
				}
			} else {
				Debug.Log ("BombUpSpan ins't 10");
			}
		}
	}

	private void spawnPosSetter2 () {
		int[ , ] emptyBlock = new int[m_SpawnNum, BlockNum];
		int i = 0, j = 0, k = 0, EmptyNum;

		// 初期化
		for (i = 0; i < m_SpawnNum; i++) {
			for (j = 0; j < BlockNum; j++)
				emptyBlock [i, j] = -1;
			m_SpawnBlockPositions[i] = -1;
			m_SpawnBlockDirections[i] = Random.Range (0, 4);
		}
		j = 0;

		for (i = 0; i < BlockNum; i++) {
			if (m_BlockisOn [i] == false) {
				emptyBlock [0, j] = i;
				j++;
			}
		}
		EmptyNum = j;
		// １つ目の出現
		m_SpawnBlockPositions [0] = emptyBlock[0, Random.Range (0, EmptyNum)];
		for (i = 0; i < EmptyNum; i++) {
			if (emptyBlock [0, i] != m_SpawnBlockPositions [0]) {
				emptyBlock [0, k] = emptyBlock [0, i];
				k++;
			}
		}
		EmptyNum = k;
		k = 0;

		// ２つ目以降の出現アルゴリズム
		for (i = 1; i < m_SpawnNum; i++) {
			for (j = 0; j < EmptyNum; j++) {
				if (emptyBlock [i - 1, j] % m_Length != m_SpawnBlockPositions [i - 1] % m_Length
				    && emptyBlock [i - 1, j] / m_Length != m_SpawnBlockPositions [i - 1] / m_Length
					&& emptyBlock [i - 1, j] >= 0) {
					emptyBlock [i, k] = emptyBlock [i - 1, j];
					k++;
				}
			}
			// 他の列・行に出現できる
			if (k > 0) {
				m_SpawnBlockPositions [i] = emptyBlock [i, Random.Range (0, k)];
				k = 0;
				for (j = 0; j < EmptyNum; j++) {
					if (emptyBlock [0, j] != m_SpawnBlockPositions [i]) {
						emptyBlock [0, k] = emptyBlock[0, j];
						k++;
					}
				}
				EmptyNum = k;

			// 他の列・行が存在しない
			} else {
				if (EmptyNum == 0)
					break;
				else {
					m_SpawnBlockPositions [i] = emptyBlock [0, Random.Range (0, EmptyNum)];
					for (j = 0; j < EmptyNum; j++) {
						if (emptyBlock [0, j] != m_SpawnBlockPositions [i]) {
							emptyBlock [0, k] = emptyBlock[0, j];
							k++;
						}
					}
					EmptyNum = k;
				}
			}
			k = 0;
		}
	}

		
	// タッチした物体をtouchObjとして返す
	private bool RaycastScrollListner(Vector3 point, out GameObject touchObj)
	{
		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		Ray ray = playercamera.ScreenPointToRay (point);
		RaycastHit hit;
		if (Physics.Raycast (ray.origin, ray.direction, out hit, (playercamera.farClipPlane - playercamera.nearClipPlane)
			, 1 << gameObject.layer)) {
			touchObj = hit.collider.gameObject;
			return true;
		}
		touchObj = null;
		return false;
	}

	// ブロックを動かす
	private void moveBlock (GameObject obj, int oldpos, int newpos) {
		//LeftTimeInit ();sta
		if (0 > oldpos || BlockNum <= oldpos || BlockNum <= newpos ) {
			Debug.Log ("Error in moveBlock(). oldpos(" + oldpos + ")->newpos(" + newpos + ")");
		} else if (newpos >= 0) {
			m_BlockisOn [oldpos] = false;
			m_BlockisOn [newpos] = true;
			iTween.MoveTo (obj, m_Pos [newpos], m_MoveSpeed);
		} else if (newpos < 0) {
			MoveCountAdder ();
			ScoreAdder ();
			m_BlockisOn [oldpos] = false;
//			obj.GetComponent<Animator> ().SetTrigger ("Movetrigger");
//			if (MoveCount < 3)
				obj.GetComponent<Animator> ().SetTrigger ("Move" + Mathf.Abs (newpos) + "trigger");
//			else {
//				obj.GetComponent<Animator> ().SetTrigger ("MovetoBomb" + Mathf.Abs (newpos) + "trigger");
//				StartCoroutine (move (obj));
//			}

			int direction = -1;
			switch (obj.name) {
			case "BlueBlock(Clone)":
				direction = 0;
				break;
			case "RedBlock(Clone)":
				direction = 1;
				break;
			case "YellowBlock(Clone)":
				direction = 2;
				break;
			case "GreenBlock(Clone)":
				direction = 3;
				break;
			default:
				break;
			}
			if (direction != -1) {
				BlockSetter (direction);
			} else {
				Debug.Log ("Error in DestoryBlock()");
			}
		}
	}

	private IEnumerator move (GameObject tmpobj) {
		if (m_BombCount <= 4) {
			yield return new WaitForSeconds (0.05f);
			iTween.MoveTo (tmpobj, m_BombPos [m_BombCount - 1], 1.0f);
			iTween.RotateTo (tmpobj, iTween.Hash ("y", 1800, "time", 1.0f));
			yield return new WaitForSeconds (0.5f);
			tmpobj.gameObject.SetActive (false);
		} else
			yield break;
	}

	// 複数のブロックを moveBlock に渡す
	private void moveBlocks (int blocknum, int count) {
		int i = 0;


		if (count == m_Length) {
			if (MoveCount < BombAddNum)
				m_RensaCount = 0;
		}
		if (MoveCount >= BombAddNum && count == m_Length) {
			StartCoroutine ("ManyBlocksRemove");
			m_RensaCount++;
		}
		for (i = 0; i < blocknum; i++) {
			moveBlock (m_blocks [i].get_obj (), m_blocks [i].get_oldpos (), m_blocks [i].get_newpos ());
		}
	}

	private IEnumerator swipetwice(string swiperesult) {
		int i = 0;
		for (i = 1; i < m_Length; i++) {
			yield return new WaitForSeconds (m_LoopWaitTime);
			if (i == m_Length - 1)
				checkBlockNum (out m_OnBlockNum);
			swipeArrower (swiperesult, i + 1);
		}
	}

	// swipeArrower を全ブロック数に対応
	private void swipeArrower (string direction, int count) {
		int i = 0, j = 0, dir = -1, param = 0, moveblocknum = 0;
		int old_pos, new_pos;
		List<GameObject> tempobjs1;

		switch (direction) {
		case ("Right"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Right"));
			dir = 1;
			param = 1;
			break;
		case ("Down"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Down"));
			dir = 2;
			param = m_Length;
			break;
		case ("Left"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Left"));
			dir = 3;
			param = -1;
			break;
		case ("Up"):
			tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
			dir = 0;
			param = -m_Length;
			break;
		default:
			tempobjs1 = new List<GameObject> ();
			break;
		}
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					break;
			}
			if (0 <= i && i < BlockNum) {
				old_pos = i;
				switch (dir) {
				// dimension x
				case (1):
				case (3):
					for (j = 0; j < m_Length; j++) {
						if (0 <= i + param && i + param < BlockNum)
						if (old_pos / m_Length == (i + param) / m_Length && !m_BlockisOn [i + param] 
							&& m_BlockisOn [old_pos]) {
							i += param;
						}
					}
					new_pos = i;
					break;
				// dimension y
				case (0):
				case (2):
					for (j = 0; j < m_Length; j++) {
						if (0 <= i + param && i + param < BlockNum)
						if ((i + param) % m_Length == old_pos % m_Length && !m_BlockisOn [i + param] 
							&& m_BlockisOn [old_pos]) {
							i += param;
						}
					}
					new_pos = i;
					break;
				default:
					new_pos = i;
					Debug.Log ("This is Error in swipeArrower");
					break;

				}
				if (((dir == 0 && new_pos / m_Length == 0) || (dir == 2 && new_pos / m_Length == (m_Length - 1))
				    || (dir == 1 && new_pos % m_Length == m_Length - 1) || (dir == 3 && new_pos % m_Length == 0)) 
					&& m_BlockisOn [old_pos]) {
					if (dir == 0 || dir == 2) {
						m_blocks [moveblocknum].set_newpos ((Mathf.Abs(new_pos - old_pos) / m_Length + 1) * -1);
					} else {
						m_blocks [moveblocknum].set_newpos ((Mathf.Abs(new_pos - old_pos) + 1) * -1);
					}
//					m_blocks [moveblocknum].set_newpos (-1);
					m_blocks [moveblocknum].set_obj (touchObj);
					m_blocks [moveblocknum].set_oldpos (old_pos);
					moveblocknum++;
				} else if (old_pos != new_pos && m_BlockisOn [old_pos]) {
					m_blocks [moveblocknum].set_newpos (new_pos);
					m_blocks [moveblocknum].set_obj (touchObj);
					m_blocks [moveblocknum].set_oldpos (old_pos);
					moveblocknum++;
				}
			} else {
				//Debug.Log ("Error in swipeArrower (). Because block isn't on plate");
			}
		} // foreach end 
		if (moveblocknum == 0 && count == 1) {
			StartCoroutine ("DirectionNotMatch");
		} else if (count > 0) {
//			moveBlocks (moveblocknum, count);
			if (count == 1) {
				FlickSound.Play ();
			}
			Debug.Log (moveblocknum);
			if (count == m_Length && m_OnBlockNum == BlockNum)
				StartCoroutine ("AllBlockRemove");
			if (count == m_Length && !m_NoBlockMoveFlg)
				StartCoroutine (appearArrowBlock (false));
			moveBlocks (moveblocknum, count);

		} else if (count == 0) {
			if (moveblocknum != 0)
				m_FreezeBlockNum++;
		}
	}

	private bool checkEndGame () {
		swipeArrower ("Up", 0);
		swipeArrower ("Right", 0);
		swipeArrower ("Down", 0);
		swipeArrower ("Left", 0);
		//if (m_BombCount == 0 && m_FreezeBlockNum == 0) {
		if (m_FreezeBlockNum == 0) {
			m_FreezeBlockNum = 0;
			return true;
		}
		m_FreezeBlockNum = 0;
		return false;
	}

	// 全消し時の動作
	private IEnumerator AllBlockRemove () {
		m_ZenkeshiImage.gameObject.SetActive (true);
		m_GameStartflg = false;
		m_EndGameflg = true;
		m_AllRemoveFlg = true;

		RemoveAllSound.Play ();
		for (int i = 0; i < 2; i++) {
			m_ScoreText.fontSize = 50;
			m_ScoreText.color = new Color ((float)255/255, (float)102/255,(float)0/255);
			Score++;
			//m_BombCounter++;
			background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
			yield return new WaitForSeconds (0.1f);
			m_ScoreText.fontSize = 30;
			//background.GetComponent<Renderer> ().material.color = originalcolor;
			yield return new WaitForSeconds (0.1f);
		}
		background.GetComponent<Renderer> ().material.color = m_AllRemoveColor;
		m_ScoreText.fontSize = 50;
		yield return new WaitForSeconds (0.5f);
		background.GetComponent<Renderer> ().material.color = originalcolor;
		m_ScoreText.fontSize = 30;
		m_ScoreText.color = Color.black;

		m_AllRemoveFlg = false;
		m_EndGameflg = false;
		m_GameStartflg = true;
		//50m_ZenkeshiImage.gameObject.SetActive (false);
	}

	// ボム使用時の動作
	private void BombAction (GameObject touchObj) {
		int i;

		if (m_BombCount > 0) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					break;
			}
			LeftTimeInit ();
			m_BlockisOn [i] = false;
			BombDowner ();
			//touchObj.SetActive (false);
			touchObj.GetComponent<Animator> ().SetTrigger("Bombtrigger");
			BombParticle.gameObject.transform.position = touchObj.gameObject.transform.position;
			switch (touchObj.tag) {
			case "Up":
				BombParticle.startColor = new Color ((float)153/255, (float)204/255, (float)238/255);
				break;
			case "Right":
				BombParticle.startColor = new Color ((float)221/255, (float)153/255, (float)153/255);
				break;
			case "Down":
				BombParticle.startColor = new Color ((float)221/255, (float)221/255, (float)68/255);
				break;
			case "Left":
				BombParticle.startColor = new Color ((float)136/255, (float)221/255, (float)170/255);
				break;
			default:
				BombParticle.startColor = Color.white;
				break;
			}
			BombParticle.Play ();
			checkBlockNum (out m_OnBlockNum);
		} else if (m_BombCount == 0) {
			NoBombSound.Play ();
		}
	}
		
	// スワイプした方向を返す
	private void swipeDirectionChecker (Vector3 startPos, Vector3 stopPos, out string swipeResult)
	{
		float moveX = stopPos.x - startPos.x;
		float moveY = stopPos.y - startPos.y;

		if (moveX > 10 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Right";
		} else if (moveX < -10 && System.Math.Abs (moveX) > System.Math.Abs (moveY)) {
			swipeResult = "Left";
		} else if (moveY > 10 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Up";
		} else if (moveY < -10 && System.Math.Abs (moveY) > System.Math.Abs (moveX)) {
			swipeResult = "Down";
		} else {
			swipeResult = null;
		}
	}

	// ブロックの出現　＆＆　透明ブロックの出現
	private IEnumerator appearArrowBlock (bool timeupflg) {
		int i = 0;

		if (!timeupflg) {
			if (!m_AllRemoveFlg)
				yield return new WaitForSeconds (0.1f);
			else 
				yield return new WaitForSeconds (0.8f);

			LeftTimeInit ();
		}
		// 透明ブロック非表示
		foreach (GameObject block in UnityEngine.GameObject.FindGameObjectsWithTag ("Clear")) {
			block.SetActive (false);
		}
		// ブロック出現
		for (i = 0; i < m_SpawnNum; i++) {
			if (m_SpawnBlockPositions [i] >= 0) {
				if (m_BlockisOn [m_SpawnBlockPositions [i]] == false) {
					m_BlockisOn [m_SpawnBlockPositions [i]] = true;
					spawnBlockPos (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
				} else
					Debug.Log ("のせたいところにブロックが既にあり spawn できない");
			}
		}

		if (GameMode != 1 && GameMode != 2) {
			if (!m_spawnNumUpFlg1) {
				// 出現個数 3 -> 2、2 -> 3
				if (m_spawnNumUpFlg0) {
					if (m_SpawnNum == 2) {
						m_SpawnNum = 3;
					} else if (m_SpawnNum == 3) {
						m_SpawnNum = 2;
					}
				}
			} else if (m_spawnNumUpFlg1 && !m_spawnNumUpFlg2) {
				m_SpawnNum = 3;

			} else if (m_spawnNumUpFlg2 && !m_spawnNumUpFlg2_1) {
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = 2;
				else if (seed == 1 || seed == 2 || seed == 3)
					m_SpawnNum = 3;
				else if (seed == 4)
					m_SpawnNum = 4;
				
			} else if (m_spawnNumUpFlg2_1 && !m_spawnNumUpFlg3_1) {
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = 3;
				else if (seed == 1 || seed == 2 || seed == 3)
					m_SpawnNum = 4;
				else if (seed == 4)
					m_SpawnNum = 5;
			} else if (m_spawnNumUpFlg3_1) {
				int seed = Random.Range (0, 5);
				if (seed == 0 || seed == 1 || seed == 2)
					m_SpawnNum = 4;
				else if (seed == 3 || seed == 4)
					m_SpawnNum = 5;
			} else if (m_spawnNumUpFlg4) {
				int seed = Random.Range (0, 5);
				if (seed == 0 ||seed == 1)
					m_SpawnNum = 4;
				else if (seed == 2 || seed == 3 || seed == 4)
					m_SpawnNum = 5;
			}  else if (m_spawnNumUpFlg5) {
				int seed = Random.Range (0, 5);
				if (seed == 0)
					m_SpawnNum = 4;
				else if (seed == 1 || seed == 2 || seed == 3 || seed == 4)
					m_SpawnNum = 5;
			}
		}
		// 次の透明ブロックの表示
		spawnPosSetter2 ();
		for (i = 0; i < m_SpawnNum; i++) {
			if (m_SpawnBlockPositions [i] >= 0)
				spawnClearBlock (m_SpawnBlockDirections [i], m_SpawnBlockPositions [i]);
		}
		checkBlockNum (out m_OnBlockNum);
		yield break;
	}

	// スワイプ方向にブロックが無い場合の処理
	private IEnumerator DirectionNotMatch () {
		m_NoBlockMoveFlg = true;
		background.GetComponent<Renderer> ().material.color = Color.yellow;
		//Handheld.Vibrate();
		yield return new WaitForSeconds (0.1f);
		if (background.GetComponent<Renderer> ().material.color != Color.gray)
			background.GetComponent<Renderer> ().material.color = originalcolor;
		m_NoBlockMoveFlg = false;
		yield break;
		//StartCoroutine (appearArrowBlock (false));
	}

	// ステージ生成
	private void stageSet ()
	{
		int i;

		NowLevel = 0;
		if (GameMode == 2) {
			m_BombMaxNum = 2;
			m_SpawnNum = 3;
			LimitTime = 3;
			m_spawnNumUpFlg1 = true;
			NowLevel = 5;
			BombUpSpan = 10;
			BombAddNum = 4;
		} else if (GameMode == 1) {
			m_SpawnNum = 4;
			BombAddNum = 3;
			BombUpSpan = 10;
		} else {
			BombAddNum = 3;
			BombUpSpan = 10;
			NowLevel = 0;
		}
		Score = 0;
		m_BombCounter = 0;
		m_Length = (int)Mathf.Sqrt (BlockNum);
		m_BlockisOn = new bool[BlockNum];
		m_Pos = new Vector3[BlockNum];
		m_BombPos = new Vector3[m_BombMaxNum];
		m_GagePos = new Vector3[BombUpSpan];
		m_BombObj = new GameObject[m_BombMaxNum];
		m_SpawnBlockPositions = new int[BlockNum];
		m_tmpPosition1 = new int[BlockNum];
		m_tmpPosition2 = new int[BlockNum];
		m_tmpPosition3 = new int[BlockNum];
		m_SpawnBlockDirections = new int[BlockNum];
		m_AllRemoveColor = new Color ((float)153 / 255, 1f, (float)102 / 255, 0.5f);


		AudioSource[] audioSources = GetComponents<AudioSource> ();
		FlickSound = audioSources [0];
		BombSound = audioSources [1];
		BombUpSound = audioSources [2];
		LevelUpSound = audioSources [3];
		NoBombSound = audioSources [4];
		RemoveAllSound = audioSources [5];
		FlickSound.clip = AudioFlick;
		BombSound.clip = AudioBomb;
		BombUpSound.clip = AudioBombUp;
		LevelUpSound.clip = AudioLevelUp;
		NoBombSound.clip = AudioNoBomb;
		RemoveAllSound.clip = AudioRemoveAll;
		m_LeftTime = LimitTime;

		m_blocks = new BlockInfo[BlockNum];
		background = GameObject.Find ("BackGround");
		originalcolor = background.GetComponent<Renderer> ().sharedMaterial.color;
		background.GetComponent<Renderer> ().material.color = originalcolor;
		slider = canvas.GetComponentsInChildren<Slider> ();
		RawImages = canvas.GetComponentsInChildren<RawImage> ();
		foreach (RawImage raw in RawImages) {
			if (raw.name == "AllBlockRemove")
				m_ZenkeshiImage = raw;
			if (raw.name == "Good")
				m_GoodImage = raw;
			if (raw.name == "Excellent")
				m_ExcellentImage = raw;
			if (raw.name == "Great")
				m_GreatImage = raw;
		}
		foreach (Text text in canvas.GetComponentsInChildren<Text>()) {
			if (text.name == "Rensa")
				m_RensaText = text;

		}
		m_RensaText.gameObject.SetActive (false);
		m_ZenkeshiImage.gameObject.SetActive (false);
		m_GoodImage.gameObject.SetActive (false);
		m_ExcellentImage.gameObject.SetActive (false);
		m_GreatImage.gameObject.SetActive (false);

		// 初期化
		for (i = 0; i < BlockNum; i++) {
			m_BlockisOn [i] = false;
			m_Pos [i] = new Vector3 (0f, 0f, 0f);
			if (i < m_BombMaxNum) 
				m_BombPos [i] = new Vector3 (0f, 0f, 0f);
			m_SpawnBlockPositions [i] = 0;
			m_tmpPosition1 [i] = 0;
			m_tmpPosition2 [i] = 0;
			m_tmpPosition3 [i] = 0;
			m_blocks [i].set_obj (null);
			m_blocks [i].set_oldpos (-1);
			m_blocks [i].set_newpos (-1);
			m_SpawnBlockDirections [i] = 0;
		}
		for (i = 0; i < BombUpSpan; i++) 
			m_GagePos [i] = new Vector3 (0f, 0f, 0f);
			
		//m_Pos [0] = this.transform.position;
		m_Pos [0] = Vector3.right *  this.transform.position.x 
			+ Vector3.up * this.transform.position.y 
			+ Vector3.forward * (this.transform.position.z);
		for (i = 1; i < BlockNum; i++) {
			m_Pos [i] = Vector3.right * (m_Pos [i - 1].x + 1.0f) + Vector3.up * m_Pos [i - 1].y 
				+ Vector3.forward * m_Pos [i - 1].z;
			if (i % (int)Mathf.Sqrt((float)BlockNum) == 0)
				m_Pos [i] = Vector3.right * (m_Pos [i - 1].x - ((int)Mathf.Sqrt((float)BlockNum ) - 1) * 1.0f) 
					+ Vector3.up * m_Pos [i - 1].y + Vector3.forward * (m_Pos [i - 1].z - 1.0f);
		}
		if (m_BombMaxNum != 2)
			for (i = 0; i < m_BombMaxNum; i++)
				m_BombPos [i] = Vector3.right * m_Pos [m_Length - 1].x / (m_BombMaxNum - 1) * i
					+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [m_Length - 1].z / (m_BombMaxNum - 1) * i + 1.3f);
		else if (m_BombMaxNum == 2)
			for (i = 0; i < m_BombMaxNum; i++)
				m_BombPos [i] = Vector3.right * m_Pos [i].x
					+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [i].z + 1.3f);

		for (i = 0; i < BombUpSpan; i++)
			m_GagePos [i] = Vector3.right * m_Pos [m_Length - 1].x / (BombUpSpan - 1) * i 
				+ Vector3.up * m_Pos [i].y + Vector3.forward * (m_Pos [m_Length - 1].z / (BombUpSpan - 1) * i + 0.8f);

		Camera playercamera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		playercamera.transform.position = Vector3.right * (m_Pos [0].x + (float)(m_Length - 1) / 2) 
			+ Vector3.up * playercamera.transform.position.y 
			+ Vector3.forward * (m_Pos [0].z - (1.2f + ((int)m_Length - 4) * 0.5f));
		playercamera.orthographicSize = m_Length;
		for (i = 0; i < BlockNum; i++) {
			m_UnderPlate = (GameObject)Instantiate (UnderPlate);
			m_UnderPlate.tag = "UnderPlate";
			m_UnderPlate.transform.position = m_Pos[i] +  Vector3.up * (m_Pos[i].y - 0.1f);
		}
		BombSet ();
		BombSpawn (m_BombCount, 1);
		spawnPosSetter2 ();
		for (i = 0; i < m_SpawnNum; i++)
			m_SpawnBlockDirections [i] = Random.Range (0, 4);
		StartCoroutine (appearArrowBlock(false));
	}

	private IEnumerator BombUpAction (GameObject target) {
//		yield return new WaitForSeconds (0.2f);
		if (m_BombCount < m_BombMaxNum) {
			iTween.MoveTo (target, m_BombPos [m_BombCount], 0.7f);
			yield return new WaitForSeconds (0.7f);
			target.gameObject.SetActive (false);
		} else {
			Vector3 tmppos = Vector3.right * m_BombPos [2].x
			         + Vector3.up * m_BombPos [2].y + Vector3.forward * (m_BombPos [2].z + 0.6f);
			iTween.MoveTo (target, tmppos, 0.7f);
			m_ScoreText.fontSize = 50;
			m_ScoreText.color = new Color ((float)255/255, (float)102/255,(float)0/255);
			yield return new WaitForSeconds (0.3f);
			Score = Score + 2;
			yield return new WaitForSeconds (0.4f);
			m_ScoreText.fontSize = 30;
			m_ScoreText.color = Color.black;
			target.gameObject.SetActive (false);

		}
		yield break;
	}

	private void BlockSetter (int direction) {
		GameObject obj;
		if (m_BombCounter >= BombUpSpan) {
			if (m_BombCounter == BombUpSpan)
				foreach (GameObject obj2 in GameObject.FindGameObjectsWithTag ("Small")) {
					StartCoroutine (BombUpAction (obj2));
				}
			if (m_BombCounter > BombUpSpan) {
				switch (direction) {
				case 0:
					obj = (GameObject)Instantiate (BlueBlock);
					obj.transform.eulerAngles = Vector3.up * 180f;
					break;
				case 1:
					obj = (GameObject)Instantiate (RedBlock);
					obj.transform.eulerAngles = Vector3.up * 270f;
					break;
				case 2:
					obj = (GameObject)Instantiate (YelloBlock);
					break;
				case 3:
					obj = (GameObject)Instantiate (GreenBlock);
					obj.transform.eulerAngles = Vector3.up * 90f;
					break;
				default:
					obj = (GameObject)Instantiate (GreenBlock);
					break;
				}
				obj.gameObject.transform.FindChild ("block").localScale = new Vector3 (0.25f, 0.25f, 0.25f);
				obj.tag = "Small";
				obj.gameObject.transform.position = m_GagePos [m_BombCounter - BombUpSpan - 1];
			}
		} else {
			switch (direction) {
			case 0:
				obj = (GameObject)Instantiate (BlueBlock);
				obj.transform.eulerAngles = Vector3.up * 180f;
				break;
			case 1:
				obj = (GameObject)Instantiate (RedBlock);
				obj.transform.eulerAngles = Vector3.up * 270f;
				break;
			case 2:
				obj = (GameObject)Instantiate (YelloBlock);
				break;
			case 3:
				obj = (GameObject)Instantiate (GreenBlock);
				obj.transform.eulerAngles = Vector3.up * 90f;
				break;
			default:
				obj = (GameObject)Instantiate (GreenBlock);
				break;
			}
			obj.gameObject.transform.FindChild ("block").localScale = new Vector3 (0.25f, 0.25f, 0.25f);
			obj.tag = "Small";
			obj.gameObject.transform.position = m_GagePos [m_BombCounter - 1];
		}
	}

	// uGUI用
	private void uguiSet() {
		foreach (Text text in canvas.GetComponentsInChildren<Text>()) {
			if (text.name == "Score") {
				m_ScoreText = text;
				m_ScoreText.text = "" +Score;
			}
			if (text.name == "MaxScore" && GameMode != 3) {
				m_MaxScoreText = text;
				m_MaxScoreText.text = "" + MaxScore;
			}
			if (text.name == "Level" && GameMode != 3) {
				m_LevelText = text;
				m_LevelText.text = "" + NowLevel;
				if (NowLevel == 1)
					m_LevelText.color = Color.blue;
				if (NowLevel == 2)
					m_LevelText.color = Color.red;
				if (NowLevel == 3)
					m_LevelText.color = Color.magenta;
				if (NowLevel == 4)
					m_LevelText.color = Color.grey;
				if (NowLevel == 5)
					m_LevelText.color = Color.white;
			}
			if (text.name == "Time") {
				//text.text = "" + LimitTime;
			}
		}
		//Slider slider = canvas.GetComponentInChildren<Slider> ();
		foreach (Slider slid in slider) {
			if (slid.name == "Slider") {
				slid.value = m_LeftTime / LimitTime;
				if (m_LeftTime <= 0) {
					checkBlockNum (out m_OnBlockNum);
					LeftTimeInit ();
					StartCoroutine (appearArrowBlock (true));
				}
//				slid.gameObject.SetActive (false);
			}
			if (slid.name == "ScoreSlider") {
				//slid.value = (float)m_BombCounter / BombUpSpan;
				slid.gameObject.SetActive (false);
			}
		}
	}

	// ブロックの Instantiate
	private void spawnClearBlock (int direction, int position) {
		switch (direction) {
		case 0:
			m_ClearBlock = (GameObject)Instantiate (ClearBlue);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 1:
			m_ClearBlock = (GameObject)Instantiate (ClearRed);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 2:
			m_ClearBlock = (GameObject)Instantiate (ClearYellow);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 3:
			m_ClearBlock = (GameObject)Instantiate (ClearGreen);
			m_ClearBlock.transform.eulerAngles = Vector3.up * 90f;
			break;
		default:
			Debug.Log ("Error in spawnClearBlock");
			break;
		}
		m_ClearBlock.tag = "Clear";
		m_ClearBlock.transform.position = Vector3.right * m_Pos [position].x + 
			Vector3.up * (m_Pos [position].y - 0.01f)+ Vector3.forward * m_Pos [position].z;
	}
	private void spawnBlockPos (int direction, int position)
	{
		switch (direction) {
		case 0:
			m_Block = (GameObject)Instantiate (BlueBlock);
			m_Block.tag = "Up";
			m_Block.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 1:
			m_Block = (GameObject)Instantiate (RedBlock);
			m_Block.tag = "Right";
			m_Block.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 2:
			m_Block = (GameObject)Instantiate (YelloBlock);
			m_Block.tag = "Down";
			m_Block.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 3:
			m_Block = (GameObject)Instantiate (GreenBlock);
			m_Block.tag = "Left";
			m_Block.transform.eulerAngles = Vector3.up * 90f;
			break;
		case 4:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "UpBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 180f;
			break;
		case 5:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "RightBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 270f;
			break;
		case 6:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "DownBlack";
			m_DownBlackBlock.transform.eulerAngles = Vector3.up * 0f;
			break;
		case 7:
			m_DownBlackBlock = (GameObject)Instantiate (BlackBlock);
			m_DownBlackBlock.tag = "LeftBlack";
			break;
		default:
			Debug.Log ("Error in spawnBlockPos");
			break;
		}
		if  (0 <= direction && direction < 4)
			m_Block.transform.position = m_Pos [position];
		else if (4 <= direction && direction < 8)
			m_DownBlackBlock.transform.position = m_Pos [position];
	}
	// ボムの初期化
	private void BombSet () {
		for (int i = 0; i < m_BombMaxNum; i++) {
			m_BombObj[i] = (GameObject)Instantiate (Bomb);
			m_BombObj[i].transform.position = m_BombPos [i];
			m_BombObj [i].SetActive (false);
			m_BombSpaceObj = (GameObject)Instantiate (BombSpace);
			m_BombSpaceObj.transform.position = m_BombPos [i];
		}
		for (int i = 0; i < BombUpSpan; i++) {
			m_BombSpaceObj = (GameObject)Instantiate (BombSpace);
			//m_BombSpaceObj.transform.position = m_GagePos [i];
			m_BombSpaceObj.gameObject.transform.localScale *= 0.5f;
			m_BombSpaceObj.transform.position = Vector3.right *  m_GagePos [i].x 
				+ Vector3.up * (m_GagePos [i].y - 0.01f) + Vector3.forward * m_GagePos[i].z;
		}
	}

	private IEnumerator ManyBlocksRemove () {
		BombAdder ();

		Debug.Log (m_RensaCount);
//		if (m_RensaCount > 1) {
//			m_RensaText.gameObject.SetActive (true);
//			m_RensaText.text = m_RensaCount + "連鎖";
//		} else 
		if (!m_AllRemoveFlg && MoveCount == 3) {
//			m_RensaText.gameObject.SetActive (false);
			m_GoodImage.gameObject.SetActive (true);
		} else {
//			m_RensaText.gameObject.SetActive (false);
			m_ScoreText.fontSize = 60;
			m_ScoreText.color = new Color ((float)255/255, (float)102/255,(float)0/255);
			if (!m_AllRemoveFlg && (4 <=MoveCount && MoveCount < 5))
				m_GoodImage.gameObject.SetActive (true);
			if (!m_AllRemoveFlg && (5 <= MoveCount && MoveCount < BlockNum / 2))
				m_GreatImage.gameObject.SetActive (true);
			if (!m_AllRemoveFlg && (BlockNum / 2 <= MoveCount))
				m_ExcellentImage.gameObject.SetActive (true);
			background.GetComponent <Renderer> ().material.color = m_AllRemoveColor;
			for (int i = 3; i < MoveCount; i++) {
				Score = Score + 2;
			}
			yield return new WaitForSeconds (0.5f);
			m_ScoreText.fontSize = 30;
			m_ScoreText.color = Color.black;
			background.GetComponent <Renderer> ().material.color = originalcolor;
		}
		yield break;
	}

	private IEnumerator StopTime () {
		LeftTimeInit ();
		m_TimeStopFlg = false;
		yield return new WaitForSeconds (5f);
	}

	private IEnumerator LevelUp () {
		m_LevelText.fontSize = 80;
		yield return new WaitForSeconds (0.3f);
		NowLevel++;
		LevelUpSound.Play ();
		yield return new WaitForSeconds (0.3f);
		m_LevelText.fontSize = 70;
	}

	// ボムの Instantiate
	private void BombSpawn (int num, int flg) {
		// 2 -> BombDowner, 1 -> BombAdder
		if (flg == 2) {
			if (m_GameStartflg)
				BombSound.Play ();
			m_BombObj [num].GetComponent<Animator> ().SetTrigger ("BombRemoveTrigger");
		}
		if (flg == 1) {
			if (m_GameStartflg) {
				BombUpSound.Play ();
			}
			m_BombObj [num - 1].SetActive (true);
			m_BombObj[num - 1].GetComponent <Animator> ().SetTrigger ("BombPopupTrigger");
		}
	}

	private void BombAdder () {
		if (m_BombCount < m_BombMaxNum) {
			m_BombCount++;
			BombSpawn (m_BombCount, 1);
		}
	}
	private void BombDowner () {
		m_BombCount--;
		BombSpawn (m_BombCount, 2);
	}

	// 残り時間の初期化
	public void LeftTimeInit () {
		m_LeftTime = LimitTime;
	}

	public static void MoveCountAdder () {
		MoveCount++;
	}

	// スコア更新
	public static void ScoreAdder () {
		Score++;
		m_BombCounter++;
	}

	// 最大スコア更新
	public static void MaxScoreUpdater () {
		MaxScore = Score;
	}
	// ユーザレベル更新
	public static void UserLevelUpdater () {
		UserLevel = NowLevel;
	}

	// スコアを減らす
	public static void ScoreDowner () {
		Score--;
	}

	// ボタンが押された時用
	public void reset () {
		Application.LoadLevel ("Main2");
	}
	public void title () {
		Application.LoadLevel ("Title");
	}

	// ゲームを終わらせる
	private IEnumerator EndGame () {
		m_GameStartflg = false;
		m_EndGameflg = true;
		background.GetComponent<Renderer> ().material.color = Color.gray;
		foreach (Slider slid in slider) {
			if (slid.name == "Slider") {
				slid.gameObject.SetActive (false);
			}
		}
		yield return new WaitForSeconds (2f);
		Application.LoadLevel ("End");
		yield break;
	}
}

/*
	// ブロックの出現場所を m_SpawnBlockPositions[i] に入れる（最大出現数３）
private void spawnPosSetter () {
	int i = 0, j = 0, k = 0, l = 0, m = 0;

	for (i = 0; i < BlockNum; i++) {
		m_SpawnBlockPositions[i] = 0;
		if (m_BlockisOn [i] == false) {
			m_tmpPosition1 [j] = i;
			j++;
		}
	}
	if (j > 0) {
		for (i = 0; i < m_SpawnNum; i++) {
			m_SpawnBlockDirections [i] = Random.Range (0, 4);
		}
		m_SpawnBlockPositions [0] = m_tmpPosition1 [Random.Range (0, j)];
		for (k = 1; k < m_SpawnNum; k++) {
			if (k == 1) {
				for (i = 0; i < j; i++) {
					if (m_tmpPosition1 [i] % m_Length != m_SpawnBlockPositions [k - 1] % m_Length
						&& m_tmpPosition1 [i] / m_Length != m_SpawnBlockPositions [k - 1] / m_Length) {
						m_tmpPosition2 [l] = m_tmpPosition1 [i];
						l++;
					}
				}
				if (l != 0) {
					m_SpawnBlockPositions [k] = m_tmpPosition2 [Random.Range (0, l)];
				} else if (l == 0) {
					for (i = 0; i < j; i++) {
						if (m_tmpPosition1 [i] != m_SpawnBlockPositions [0]) {
							m_tmpPosition2 [l] = m_tmpPosition1 [i];
							l++;
						}
					}
					if (l != 0) {
						m_SpawnBlockPositions [k] = m_tmpPosition2 [Random.Range (0, l)];
						for (i = 0; i < l; i++) {
							if (m_tmpPosition2 [i] != m_SpawnBlockPositions [k]) {
								m_tmpPosition3 [m] = m_tmpPosition2 [i];
								m++;
							}
							if (m != 0) {
								m_SpawnBlockPositions [k + 1] = m_tmpPosition3 [Random.Range (0, m)];
							} else if (m == 0) {
								m_SpawnBlockPositions [k + 1] = -1;
							}
						}
					} else {
						m_SpawnBlockPositions [k] = -1;
						m_SpawnBlockPositions [k + 1] = -1;
					}
				}
			}

			if (k == 2 && m_SpawnBlockPositions [k] != -1) {
				for (i = 0; i < l; i++) {
					if (m_tmpPosition2 [i] % m_Length != m_SpawnBlockPositions [k - 1] % m_Length
						&& m_tmpPosition2 [i] / m_Length != m_SpawnBlockPositions [k - 1] / m_Length) {
						m_tmpPosition3 [m] = m_tmpPosition2 [i];
						m++;
					}
				}
				if (m != 0)
					m_SpawnBlockPositions [k] = m_tmpPosition3 [Random.Range (0, m)];
				if (m == 0) {
					for (i = 0; i < l; i++) {
						if (m_tmpPosition2 [i] != m_SpawnBlockPositions [k - 1]) {
							m_tmpPosition3 [m] = m_tmpPosition2 [i];
							m++;
						}
					}
					if (m != 0)
						m_SpawnBlockPositions [k] = m_tmpPosition3 [Random.Range (0, m)];
					else if (m == 0)
						m_SpawnBlockPositions [k] = -1;
				}
			}
		}
	} else if (j == 0)
		for (i = 0; i < m_SpawnNum; i++)
			m_SpawnBlockPositions [i] = -1;
} 

// スワイプ方向にあるブロックの情報を moveBlocks に渡す
private void swipeArrower (string direction, int count) {
	int i = 0, j = 0;

	if (direction.Equals("Right")) {
		List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Right"));
		List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("LeftBlack"));
		tempobjs1.AddRange (tempobjs2);
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
					break;
			}
			if (i >= 0 && i < BlockNum && m_BlockisOn[i])
			if (BlockNum == 16) {
				if (i == 3 || i == 7 || i == 11 || i == 15) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
					//moveBlock (touchObj, i, -1);
				} else if (!m_BlockisOn [i + 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
					//moveBlock (touchObj, i, -1);
				} else if (!m_BlockisOn [i + 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
					if (!m_BlockisOn [i + 2]) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
						//moveBlock (touchObj, i, -1);
					} else {
						m_blocks [j].set_newpos (i + 1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
						//moveBlock (touchObj, i, i + 1);
					}
				} else if (!m_BlockisOn [i + 1] && (i == 0 || i == 4 || i == 8 || i == 12)) {
					if (!m_BlockisOn [i + 2]) {
						if (!m_BlockisOn [i + 3]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
							//moveBlock (touchObj, i, -1);
						} else {
							m_blocks [j].set_newpos (i + 2);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
							//moveBlock (touchObj, i, i + 2);
						}
					} else {
						m_blocks [j].set_newpos (i + 1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
						//moveBlock (touchObj, i, i + 1);
					}
				} 
			}
			if (j == 1 && count == 1)
				FlickSound.Play ();
		}

	} else if (direction.Equals("Left")) {
		List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Left"));
		List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("RightBlack"));
		tempobjs1.AddRange (tempobjs2);
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
					break;
			}
			if (i >= 0 && i < BlockNum && m_BlockisOn[i])
			if (BlockNum == 16) {
				if (i == 0 || i == 4 || i == 8 || i == 12) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i - 1] && (i == 1 || i == 5 || i == 9 || i == 13)) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i - 1] && (i == 2 || i == 6 || i == 10 || i == 14)) {
					if (!m_BlockisOn [i - 2]) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else {
						m_blocks [j].set_newpos (i - 1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				} else if (!m_BlockisOn [i - 1] && (i == 3 || i == 7 || i == 11 || i == 15)) {
					if (!m_BlockisOn [i - 2]) {
						if (!m_BlockisOn [i - 3]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i - 2);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else {
						m_blocks [j].set_newpos (i - 1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				}
			}
			if (j == 1 && count == 1)
				FlickSound.Play ();
		}

	} else if (direction.Equals("Up")) {
		List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Up"));
		List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("DownBlack"));
		tempobjs1.AddRange (tempobjs2);
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
					break;
			}
			if (i >= 0 && i < BlockNum && m_BlockisOn[i])
			if (BlockNum == 16) {
				if (0 <= i && i <= 3) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i - 4] && (4 <= i && i <= 7)) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i - 4] && (8 <= i && i <= 11)) {
					if (!m_BlockisOn [i - 8]) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else {
						m_blocks [j].set_newpos (i - 4);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				} else if (!m_BlockisOn [i - 4] && (12 <= i && i <= 15)) {
					if (!m_BlockisOn [i - 8]) {
						if (!m_BlockisOn [i - 12]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i - 8);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else {
						m_blocks [j].set_newpos (i - 4);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				}
			}
			if (j == 1 && count == 1)
				FlickSound.Play ();
		}

	} else if (direction.Equals("Down")) {
		List<GameObject> tempobjs1 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Down"));
		List<GameObject> tempobjs2 = new List<GameObject> (GameObject.FindGameObjectsWithTag ("UpBlack"));
		tempobjs1.AddRange (tempobjs2);
		foreach (GameObject touchObj in tempobjs1) {
			for (i = 0; i < BlockNum; i++) {
				if ((Mathf.RoundToInt (m_Pos [i].x) == Mathf.RoundToInt (touchObj.transform.position.x)) 
					&& (Mathf.RoundToInt (m_Pos [i].z) == Mathf.RoundToInt (touchObj.transform.position.z)))
					//if (m_Pos [i] == objs.transform.position)
					break;
			}
			if (i >= 0 && i < BlockNum && m_BlockisOn[i])
			if (BlockNum == 16) {
				if (12 <= i && i <= 15) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i + 4] && (8 <= i && i <= 11)) {
					m_blocks [j].set_newpos (-1);
					m_blocks [j].set_obj (touchObj);
					m_blocks [j].set_oldpos (i);
					j++;
				} else if (!m_BlockisOn [i + 4] && (4 <= i && i <= 7)) {
					if (!m_BlockisOn [i + 8]) {
						m_blocks [j].set_newpos (-1);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					} else {
						m_blocks [j].set_newpos (i + 4);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				} else if (!m_BlockisOn [i + 4] && (0 <= i && i <= 3)) {
					if (!m_BlockisOn [i + 8]) {
						if (!m_BlockisOn [i + 12]) {
							m_blocks [j].set_newpos (-1);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						} else {
							m_blocks [j].set_newpos (i + 8);
							m_blocks [j].set_obj (touchObj);
							m_blocks [j].set_oldpos (i);
							j++;
						}
					} else {
						m_blocks [j].set_newpos (i + 4);
						m_blocks [j].set_obj (touchObj);
						m_blocks [j].set_oldpos (i);
						j++;
					}
				}
			}
			if (j == 1 && count == 1)
				FlickSound.Play ();
		}
	}
	if (j == 0 && count == 1) {
		StartCoroutine ("DirectionNotMatch");
	} else {
		moveBlocks (j, count);
		if (count == 1) 
			StartCoroutine (appearArrowBlock(false));
		// 全消し追加
		if (count == m_Length && m_OnBlockNum == BlockNum && AllRemoveFlg) {
			StartCoroutine ("AllBlockRemove");
		}
	}
}

*/
